import express from 'express';
import expressSession from 'express-session';
import { Client } from 'pg';
import dotenv from 'dotenv';
import { UserController} from './UserController';
import { timetableRoutes } from './timetableRoutes';
import { coursesRoutes } from './coursesRoutes';
import { configureRoutes } from './configureRoutes';
import path from 'path';
import { UserService } from './UserService';

const app = express();

/* environment config */
dotenv.config();

/* database initiation */
export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD
})
client.connect();

/* client-server data exchange protocols */
app.use(express.json());
app.use(express.urlencoded({ extended: true }))

/* express session settings */
app.use(expressSession({
    secret: 'timetable',
    resave: true,
    saveUninitialized: true
}));

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, '/views'));

app.use('/static', express.static(__dirname + '/static'));

app.use('/', timetableRoutes);

app.use('/', coursesRoutes);

app.use('/', configureRoutes);

export const userService = new UserService(client);
export const userController = new UserController(userService);

import { routes } from './routes';
app.use('/', routes);

app.get('/404', (req, res) => {
    const page = '404';
    res.status(404).render('public/404', { page: page });
})

app.use((req, res) => {
    res.status(404).redirect('/404');
});

const PORT = 8080;
app.listen(PORT, function () {
    console.log('Listening at PORT 8080.')
});