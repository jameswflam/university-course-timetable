export interface User{
    student_id?:number
    first_name: string
    last_name: string
    username:string
    password:string
}

export interface Course {
    id: number
    subject: string
    code: string
    name: string
    credit: number
}

export interface Activity {
    courseId: number
    name: string
    weekday: string
    startTime: string
    endTime: string
    venue: string
    lecturer: string
}