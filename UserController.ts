import { Request, Response } from 'express';
import { checkPassword, hashPassword } from './hash';
import { User } from './models';
import { UserService } from './UserService';

export class UserController {
    constructor(private userService: UserService) { }

    renderIndexPage = (req: Request, res: Response) => {
        const page = 'index';
        res.render('public/index', { page: page });
    }

    renderLoginPage = (req: Request, res: Response) => {
        const page = 'login';
        res.render('public/login', { page: page });
    }

    renderSignUpPage = (req: Request, res: Response) => {
        const page = 'signup';
        res.render('public/signup', { page: page });
    }

    renderConfigPage = (req: Request, res: Response) => {
        const page = 'configure';
        res.render('protected/configure', { page: page, user: req.session['user'] });
    }

    renderRegisteredPage = (req: Request, res: Response) => {
        const page = 'registered';
        res.render('protected/registered', { page: page, user: req.session['user'] });
    }

    renderEditProfilePage = (req: Request, res: Response) => {
        const page = 'editprofile';
        res.render('protected/editprofile', { page: page, user: req.session['user'] });
    }

    login = async (req: Request, res: Response) => {
        const { username, password } = req.body;

        let users: User[] = await this.userService.getUser(username);
        const user = users[0];

        if (user && await checkPassword(password, user.password)) {
            req.session['user'] = user;
            req.session['student_id'] = user.student_id;
            res.status(200).json({ success: true });
        } else {
            res.status(401).json({ success: false, msg: 'Incorrect username/password.' });
        }
    }

    signup = async (req: Request, res: Response) => {
        const { first_name, last_name, username, password, confirm_password } = req.body;

        if (password !== confirm_password) {
            res.status(400).json({ success: false, msg: 'Passwords does not match' });
        } else {
            const numOfMatchedUser = (await this.userService.getUser(username)).length;
            if (numOfMatchedUser !== 0) {
                res.status(400).json({ success: false, msg: 'Username already exist' });
            }
            else {
                let passwordHash = await hashPassword(password);
                await this.userService.createUser(first_name, last_name, username, passwordHash);
                res.json({ success: true });
            }
        }
    }

    logout = async (req: Request, res: Response) => {
        req.session.destroy((err) => { });
        res.redirect('/');
    }

    studentInfo = async (req: Request, res: Response) => {
        let studentInfo = await this.userService.getUserInfo(req);
        res.json(studentInfo);
    }

    updateStudentInfo = async (req: Request, res: Response) => {
        let formObj = await req.body;

        let resultPassword = await this.userService.getPasswordHash(req);
        let bool = await checkPassword(formObj.old_password, resultPassword.password);
        if (!bool) {
            res.status(400).json({ success: false });
            return;
        }
        let passwordHash = await hashPassword(formObj.password);
        await this.userService.setStudentInfo(formObj.first_name, formObj.last_name, passwordHash, req);
        res.status(200).json({ success: true });
    }
}