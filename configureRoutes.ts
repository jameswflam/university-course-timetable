import express from 'express';
import { client } from './app';
import { generateCourseJSON, Course } from './lib';
// import { isLoggedIn } from './guard';

export const configureRoutes = express.Router();

configureRoutes.put('/configurations', async (req, res) => {
    const body = req.body;
    const studentId = req.session['student_id'];

    /* ---------------------credits--------------------*/
    // /* save credit in students */
    // await client.query(`UPDATE students SET config_credit = $1
    //     WHERE student_id = $2`, [body.credits, studentId]);

    /* ---------------------Must take courses--------------------*/
    /* get course ids in must takes */
    const mustTakes = body.mustTake;
    let values = [];
    let conditionString = '';
    let conditionStrings = [];
    let count = 1;

    for (let key in mustTakes) {
        const fullCourseCode = mustTakes[key];
        const subject = (fullCourseCode.split(' '))[0];
        const code = (fullCourseCode.split(' '))[1];

        values.push(subject);
        values.push(code);

        conditionStrings.push(`(subjects.subject_id = LOWER($${count++}) AND courses.course_code = LOWER($${count++}))`);
    }

    conditionString = conditionStrings.join(' OR ');

    const courseIds = (await client.query(`SELECT courses.course_id
        FROM courses
        JOIN subjects ON subjects.subject_id = courses.subject_id 
        ${conditionString === '' ? '' : 'WHERE'} ${conditionString}`, values)).rows;


    /* put must take courses into database */
    values = [];
    count = 1;
    conditionStrings = [];
    conditionString = '';

    for (let course of courseIds) {
        const courseId = course.course_id;
        conditionStrings.push(`($${count++}, $${count++})`);
        values.push(studentId);
        values.push(courseId);
    }

    conditionString = conditionStrings.join(', ');

    // first remove old must_take_courses
    await client.query(`DELETE FROM must_take_courses WHERE student_id = $1`, [studentId]);

    // insert data into must_take_courses
    await client.query(`INSERT INTO must_take_courses (student_id, course_id)
    VALUES ${conditionString}`, values);

    /* ---------------------Must take area--------------------*/
    // first remove old must_take_courses
    await client.query(`DELETE FROM must_take_common_core_areas WHERE student_id = $1`, [studentId]);

    // insert data into must_take_common_core_areas
    values = [];
    count = 1;
    conditionStrings = [];
    conditionString = '';
    const areas = body.mustTakeArea;

    for (let key in areas) {
        conditionStrings.push(`($${count++}, LOWER($${count++}))`);
        values.push(studentId);
        values.push(areas[key] === 'sat' ? 's&t' : areas[key]);
    }

    conditionString = conditionStrings.join(', ');

    // insert data
    await client.query(`INSERT INTO must_take_common_core_areas (student_id, common_core_area_id) 
    ${conditionString === '' ? '' : 'VALUES'} ${conditionString}`, values);

    //TODO save common area priorities
    //TODO save time off data

    res.json({ success: true });
});

configureRoutes.get('/mustTakes', async (req, res) => {
    let mustTakes = req.query.mustTake as string | string[] | undefined;
    if (!mustTakes) return res.json({ success: false, msg: 'No must take course was specified.' });

    /* convert single argument to string for later use */
    if (typeof mustTakes === 'string') mustTakes = [mustTakes] as string[];

    /* convert query parameters to string and array for SQL query */
    const mustTakeCourses = await getCourses(mustTakes);
    const mustTakeCourseObjs: Course[] = await generateCourseJSON(mustTakeCourses);

    return res.json({ 
        success: true,
        found: mustTakeCourses.length,
        courses: mustTakeCourseObjs
    });
});

async function getCourses(courses: Object) {
    let values = [];
    let conditionString = '';
    let conditionStrings = [];
    let count = 1;

    for (let key in courses) {
        const fullCourseCode = courses[key];
        const subject = (fullCourseCode.split(' '))[0];
        const code = (fullCourseCode.split(' '))[1];

        values.push(subject);
        values.push(code);

        conditionStrings.push(`(subjects.subject_id = LOWER($${count++}) AND courses.course_code = LOWER($${count++}))`);
    }

    conditionString = conditionStrings.join(' OR ');

    const resultCourses = (await client.query(`SELECT DISTINCT ON (courses.course_id) courses.course_id as id, 
        subjects.subject_id as subject, course_code as code, course_name as title, credit, 
        common_core_areas.common_core_area_id as common_core_area
        FROM courses
        LEFT JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
        LEFT JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
        JOIN subjects ON courses.subject_id = subjects.subject_id
        JOIN departments ON subjects.department_id = departments.department_id
        ${conditionString === '' ? '' : 'WHERE'} ${conditionString}`, values)).rows;

    return resultCourses;
}