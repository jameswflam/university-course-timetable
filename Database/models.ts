export interface Instructor {
    first_name: string,
    last_name?: string
}
export interface Activity {
    course_subject: string,
    course_code: string,
    name: string,
    type: number,
    instructor: string,
    venue: string,
    weekday: number,
    start_time: string,
    end_time: string
}

export enum ActivityType{
    Lecture = 0,
    Tutorial,
    Lab,
    Research
}

export enum ActivityTypeId{
    lecture_id = 0,
    tutorial_id,
    lab_id,
    research_id
}

export enum ActivityTypeTableName{
    lectures = 0,
    tutorials,
    labs,
    researches
}

export enum ActivityTypeName{
    lecture_name = 0,
    tutorial_name,
    lab_name,
    research_name
}
export interface ActivityWithTypeDetails{
    name: string,
    subject: string,
    course_code: string,
    instructor_name: string
}
export interface ActivityWithType{
    name: string,
    course_id: number,
    instructor_id: number
}