import departments from './departments.json';
import subjects from './subjects.json';
import courses from './courses.json';
import activities from './activities.json';
import commonCoreAreas from './common_core_areas.json';
import commonCoreCourses from './common_core_courses.json';
import jsonfile from "jsonfile";
async function main(){
    for(let department of departments){
        department.name = department.name.toLowerCase();
    }
    jsonfile.writeFile('./departments.json', departments, { spaces: 4 });
    
    for(let subject of subjects){
        subject.subject_id = subject.subject_id.toLowerCase();
        subject.subject_name = subject.subject_name.toLowerCase();
    }
    jsonfile.writeFile('./subjects.json', subjects, { spaces: 4 });

    for(let course of courses){
        course.subject = course.subject.toLowerCase();
        course.code = course.code.toLowerCase();
    }
    jsonfile.writeFile('./courses.json', courses, { spaces: 4 });
    
    for(let activity of activities){
        activity.course_subject = activity.course_subject.toLowerCase();
        activity.course_code = activity.course_code.toLowerCase();
    }
    jsonfile.writeFile('./activities.json', activities, { spaces: 4 });
    
    for(let commonCoreArea of commonCoreAreas){
        commonCoreArea.id = commonCoreArea.id.toLowerCase();
        commonCoreArea.name = commonCoreArea.name.toLowerCase();
    }
    jsonfile.writeFile('./common_core_areas.json', commonCoreAreas, { spaces: 4 });
    
    for(let commonCoreCourse of commonCoreCourses){
        commonCoreCourse.area = commonCoreCourse.area.toLowerCase();
        commonCoreCourse.code = commonCoreCourse.code.toLowerCase();
        commonCoreCourse.subject = commonCoreCourse.subject.toLowerCase();
    }
    jsonfile.writeFile('./common_core_courses.json', commonCoreCourses, { spaces: 4 });
}
main();
