CREATE TABLE common_core_areas(
    common_core_area_id VARCHAR(10) PRIMARY KEY,
    common_core_area_name VARCHAR(255) NOT NULL
);

CREATE TABLE departments(
    department_id SERIAL PRIMARY KEY,
    department_name VARCHAR(255) NOT NULL
);

CREATE TABLE subjects(
    subject_id VARCHAR(10) PRIMARY KEY,
    subject_name VARCHAR(255) NOT NULL,
    department_id INTEGER NOT NULL,
    FOREIGN KEY (department_id) REFERENCES departments(department_id)
);

CREATE TABLE courses(
    course_id SERIAL PRIMARY KEY,
    subject_id VARCHAR(10) NOT NULL,
    course_code VARCHAR(10) NOT NULL,
    course_name VARCHAR(255) NOT NULL,
    course_name_lower VARCHAR(255) NOT NULL,
    credit INTEGER NOT NULL,    
    FOREIGN KEY (subject_id) REFERENCES subjects(subject_id)
);

CREATE TABLE common_core_courses(
    course_id INTEGER NOT NULL,
    common_core_area_id VARCHAR(10) NOT NULL,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (common_core_area_id) REFERENCES common_core_areas(common_core_area_id)
);

CREATE TABLE instructors(
    instructor_id SERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL
);

CREATE TABLE lectures(
    lecture_id SERIAL PRIMARY KEY,
    lecture_name VARCHAR(10) NOT NULL,
    course_id INTEGER NOT NULL,
    instructor_id INTEGER,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (instructor_id) REFERENCES instructors(instructor_id)
);

CREATE TABLE tutorials(
    tutorial_id SERIAL PRIMARY KEY,
    tutorial_name VARCHAR(10) NOT NULL,
    course_id INTEGER NOT NULL,
    instructor_id INTEGER,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (instructor_id) REFERENCES instructors(instructor_id)
);

CREATE TABLE labs(
    lab_id SERIAL PRIMARY KEY,
    lab_name VARCHAR(10) NOT NULL,
    course_id INTEGER NOT NULL,
    instructor_id INTEGER,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (instructor_id) REFERENCES instructors(instructor_id)
);

CREATE TABLE researches(
    research_id SERIAL PRIMARY KEY,
    research_name VARCHAR(10) NOT NULL,
    course_id INTEGER NOT NULL,
    instructor_id INTEGER,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (instructor_id) REFERENCES instructors(instructor_id)
);

CREATE TABLE sessions(
    session_id SERIAL PRIMARY KEY,
    venue VARCHAR(255),
    weekday INTEGER NOT NULL,
    start_time TIME,
    end_time TIME,
    lecture_id INTEGER,
    tutorial_id INTEGER,
    lab_id INTEGER,
    research_id INTEGER,
    FOREIGN KEY (lecture_id) REFERENCES lectures(lecture_id),
    FOREIGN KEY (tutorial_id) REFERENCES tutorials(tutorial_id),
    FOREIGN KEY (lab_id) REFERENCES labs(lab_id),
    FOREIGN KEY (research_id) REFERENCES researches(research_id)
);

CREATE TABLE students(
    student_id SERIAL PRIMARY KEY,
    first_name VARCHAR(255) NOT NULL,
    last_name VARCHAR(255) NOT NULL,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL
);

CREATE TABLE registered_courses(
    registered_course_id SERIAL PRIMARY KEY,
    student_id INTEGER NOT NULL,
    course_id INTEGER NOT NULL,
    session_id INTEGER NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (session_id) REFERENCES sessions(session_id)
);

CREATE TABLE must_take_common_core_areas(
    student_id INTEGER NOT NULL,
    common_core_area_id VARCHAR(10) NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id),
    FOREIGN KEY (common_core_area_id) REFERENCES common_core_areas(common_core_area_id)
);

CREATE TABLE config_timeslots(
    student_id INTEGER NOT NULL,
    weekday INTEGER NOT NULL,
    start_time TIME NOT NULL,
    end_time TIME NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id)
);

CREATE TABLE course_preferences(
    student_id INTEGER NOT NULL,
    course_id INTEGER NOT NULL,
    preference INTEGER NOT NULL,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (student_id) REFERENCES students(student_id)
);

CREATE TABLE must_take_courses(
    student_id INTEGER NOT NULL,
    course_id INTEGER NOT NULL,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (student_id) REFERENCES students(student_id)
);

CREATE TABLE starred_courses(
    student_id INTEGER NOT NULL,
    course_id INTEGER NOT NULL,
    FOREIGN KEY (course_id) REFERENCES courses(course_id),
    FOREIGN KEY (student_id) REFERENCES students(student_id)
);

CREATE TABLE timetables(
    timetable_id SERIAL PRIMARY KEY,
    student_id INTEGER NOT NULL,
    FOREIGN KEY (student_id) REFERENCES students(student_id)
);

CREATE TABLE timetable_session(
    timetable_id INTEGER NOT NULL,
    session_id INTEGER NOT NULL,
    FOREIGN KEY (timetable_id) REFERENCES timetables(timetable_id),
    FOREIGN KEY (session_id) REFERENCES sessions(session_id)
);