import express from 'express';
import { client } from './app';
import { Course } from './lib';
import { isLoggedIn } from './guard';
import { generateCourseJSON } from './lib';

export const coursesRoutes = express.Router();

coursesRoutes.get('/courses-page', isLoggedIn, async (req, res) => {
    const page = 'courses';
    const schools = (await client.query('SELECT department_name FROM departments;')).rows.map(row => row.department_name);

    res.render('protected/courses', {
        page: page,
        user: req.session['user'],
        schools: schools,
    });
})

coursesRoutes.get('/schools', async (req, res) => {
    const schools = (await client.query('SELECT department_name as name from departments')).rows;

    res.json({
        success: true,
        schools: schools
    });
})

coursesRoutes.get('/subjects', async (req, res) => {
    let schoolFilters = req.query.schoolFilter as string | string[] | undefined;
    let areaFilters = req.query.areaFilter as string | string[] | undefined;

    /* convert single argument to string for later use */
    if (typeof schoolFilters === 'string') schoolFilters = [schoolFilters];
    if (typeof areaFilters === 'string') areaFilters = [areaFilters] as string[];

    const conditions: string[] = [];
    let count = 1;
    const values: string[] = [];

    let schoolFilterString = '';
    if (schoolFilters) {
        const schoolConditions: string[] = [];
        for (let filter of schoolFilters) {
            schoolConditions.push(`departments.department_name = LOWER($${count++})`);
            values.push(filter);
        }
        if (schoolConditions.length) schoolFilterString = '(' + schoolConditions.join(' OR ') + ')';
        conditions.push(schoolFilterString);
    }

    let areaFilterString = '';
    if (areaFilters) {
        const areaConditions: string[] = [];
        for (let filter of areaFilters) {
            areaConditions.push(`common_core_areas.common_core_area_id = LOWER($${count++})`);
            values.push(filter);
        }
        if (areaConditions.length) areaFilterString = '(' + areaConditions.join(' OR ') + ')';
        conditions.push(areaFilterString);
    }

    const conditionString = conditions.join(' AND ');

    const subjects = (await client.query(`SELECT DISTINCT ON (subjects.subject_id) subjects.subject_id as id
        FROM subjects
        JOIN departments ON subjects.department_id = departments.department_id
        JOIN courses ON courses.subject_id = subjects.subject_id
        LEFT JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
        LEFT JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
        ${conditionString === '' ? '' : 'WHERE'} ${conditionString} ORDER BY subjects.subject_id ASC;`, values)).rows;

    if (subjects) {
        if (subjects.length) {
            return res.json({
                success: true,
                found: subjects.length,
                subjects: subjects
            });
        }
    }

    return res.json({
        success: false,
        found: 0,
        subjects: []
    })
});

coursesRoutes.get('/courses', async (req, res) => {
    let commonCoreFilter = req.query.commonCoreFilter as string | undefined;
    let areaFilters = req.query.areaFilter as string | undefined;

    if (commonCoreFilter === 'true' || areaFilters) {
        const url = req.originalUrl;
        const questionMarkIndex = url.match(/\?/)?.index;

        let newPath = '/common-cores'
        if (questionMarkIndex) newPath = newPath + url.slice(questionMarkIndex);
        return res.redirect(newPath);
    }

    let schoolFilters = req.query.schoolFilter as string[] | string | undefined;
    let subjectFilters = req.query.subjectFilter as string[] | string | undefined;
    let subjectIDs = req.query.subjectID as string[] | string | undefined;
    let courseCodes = req.query.courseCode as string[] | string | undefined;

    /* convert single argument to string for later use */
    if (typeof schoolFilters === 'string') schoolFilters = [schoolFilters] as string[];
    if (typeof subjectFilters === 'string') subjectFilters = [subjectFilters] as string[];
    if (typeof subjectIDs === 'string') subjectIDs = [subjectIDs] as string[];
    if (typeof courseCodes === 'string') courseCodes = [courseCodes] as string[];

    /* convert query parameters to string and array for SQL query */
    const conditions: string[] = [];
    let count = 1;
    const values: string[] = [];

    let schoolFilterString = ''
    if (schoolFilters) {
        const schoolConditions: string[] = [];
        for (let filter of schoolFilters) {
            schoolConditions.push(`departments.department_name = LOWER($${count++})`);
            values.push(filter);
        }
        if (schoolConditions.length) schoolFilterString = '(' + schoolConditions.join(' OR ') + ')';
        conditions.push(schoolFilterString);
    }

    let subjectFilterString = '';
    if (subjectFilters) {
        const subjectConditions: string[] = [];
        for (let filter of subjectFilters) {
            subjectConditions.push(`subjects.subject_id = LOWER($${count++})`);
            values.push(filter);
        }
        if (subjectConditions.length) subjectFilterString = '(' + subjectConditions.join(' OR ') + ')';
        conditions.push(subjectFilterString);
    }

    const conditionString = conditions.join(' AND ');

    /* query SQL database to get courses */
    let courses = [];

    if (subjectIDs && courseCodes) {
        for (let index in subjectIDs) {
            const result = await client.query(`SELECT courses.course_id as id, subjects.subject_id as subject, 
            course_code as code, course_name as title, credit 
            FROM courses
            JOIN subjects ON courses.subject_id = subjects.subject_id
            WHERE subjects.subject_id = LOWER($1) AND course_code = LOWER($2)`, [subjectIDs[index], courseCodes[index]])
            const course = result.rows[0];
            if (course) courses.push(course)
        }
    } else {
        courses = (await client.query(`SELECT courses.course_id as id, subjects.subject_id as subject, 
            course_code as code, course_name as title, credit 
            FROM courses
            JOIN subjects ON courses.subject_id = subjects.subject_id
            JOIN departments ON subjects.department_id = departments.department_id
            ${conditionString === '' ? '' : 'WHERE'} ${conditionString} ORDER BY courses.course_id ASC;`, values)).rows;
        // ASC LIMIT 20 OFFSET 20, possible add scrollSpy?
    }

    /* form the JSON object with courses array*/
    const courseObjs: Course[] = await generateCourseJSON(courses);

    if (courseObjs.length) {
        return res.json({
            success: true,
            found: courseObjs.length,
            courses: courseObjs
        });
    }

    return res.json({
        success: false,
        found: 0,
        courses: []
    })
})

coursesRoutes.get('/common-cores', async (req, res) => {
    let areaFilters = req.query.areaFilter as string | string[] | undefined;
    let schoolFilters = req.query.schoolFilter as string[] | string | undefined;
    let subjectFilters = req.query.subjectFilter as string[] | string | undefined;
    let codeFilters = req.query.codeFilter as string[] | string | undefined;

    if (typeof areaFilters === 'string') areaFilters = [areaFilters] as string[];
    if (typeof schoolFilters === 'string') schoolFilters = [schoolFilters] as string[];
    if (typeof subjectFilters === 'string') subjectFilters = [subjectFilters] as string[];
    if (typeof codeFilters === 'string') codeFilters = [codeFilters] as string[];

    /* construct the condition string for database query */
    const conditions: string[] = [];
    let count = 1;
    const values: string[] = [];

    let areaConditionString = '';
    if (areaFilters) {
        const areaConditions: string[] = [];
        for (let filter of areaFilters) {
            areaConditions.push(`common_core_areas.common_core_area_id = LOWER($${count++})`);
            values.push(filter);
        }
        areaConditionString = '(' + areaConditions.join(' OR ') + ')';
        conditions.push(areaConditionString);
    }

    let schoolFilterString = ''
    if (schoolFilters) {
        const schoolConditions: string[] = [];
        for (let filter of schoolFilters) {
            schoolConditions.push(`departments.department_name = LOWER($${count++})`);
            values.push(filter);
        }
        if (schoolConditions.length) schoolFilterString = '(' + schoolConditions.join(' OR ') + ')';
        conditions.push(schoolFilterString);
    }

    let subjectFilterString = '';
    if (subjectFilters) {
        const subjectConditions: string[] = [];
        for (let filter of subjectFilters) {
            subjectConditions.push(`subjects.subject_id = LOWER($${count++})`);
            values.push(filter);
        }
        if (subjectConditions.length) subjectFilterString = '(' + subjectConditions.join(' OR ') + ')';
        conditions.push(subjectFilterString);
    }

    let codeFilterString = '';
    if (codeFilters) {
        const codeConditions: string[] = [];
        for (let filter of codeFilters) {
            codeConditions.push(`courses.course_code = LOWER($${count++})`);
            values.push(filter);
        }
        if (codeConditions.length) codeFilterString = '(' + codeConditions.join(' OR ') + ')';
        conditions.push(codeFilterString);
    }
    
    const conditionString = conditions.join(' AND ');

    /* query the database */
    const courses = (await client.query(`SELECT DISTINCT ON (courses.course_id) courses.course_id as id, subjects.subject_id as subject, course_code as code, 
        course_name as title, credit, common_core_areas.common_core_area_id as common_core_area
        FROM courses
        JOIN common_core_courses ON common_core_courses.course_id = courses.course_id
        JOIN common_core_areas ON common_core_areas.common_core_area_id = common_core_courses.common_core_area_id
        JOIN subjects ON courses.subject_id = subjects.subject_id
        JOIN departments ON subjects.department_id = departments.department_id
        ${conditionString === '' ? '' : 'WHERE'} ${conditionString} ORDER BY courses.course_id ASC;`, values)).rows;

    /* form JSON object from courses array */
    const courseObjs: Course[] = await generateCourseJSON(courses);
    
    if (courseObjs.length) {
        return res.json({
            success: true,
            found: courseObjs.length,
            courses: courseObjs
        });
    }

    return res.json({
        success: false,
        found: 0,
        courses: []
    })
})

coursesRoutes.put('/starredCourse', async (req, res) => {
    const body = req.body;
    const subject = body.subject;
    const code = body.code;
    const studentId = req.session['student_id'];

    // get course code
    const getCourseResult = await client.query(`SELECT courses.course_id
        FROM courses
        JOIN subjects ON subjects.subject_id = courses.subject_id
        WHERE subjects.subject_id = LOWER($1) AND courses.course_code = LOWER($2)`, 
        [subject, code]);

    if (getCourseResult.rowCount === 0) {
        return res.json({ success: false, msg: `Cannot found course ${subject.toUpperCase()}-${code.toUpperCase()}`});
    }
    
    const courseId = getCourseResult.rows[0].course_id;
    
    // insert into starred courses table
    await client.query(`INSERT INTO starred_courses (student_id, course_id)
        VALUES ($1, $2)`, [studentId, courseId]);

    return res.json({ success: true });
});

coursesRoutes.delete('/starredCourse', async (req, res) => {
    const body = req.body;
    const subject = body.subject;
    const code = body.code;
    const studentId = req.session['student_id'];

    // get course code
    const getCourseResult = await client.query(`SELECT courses.course_id
        FROM courses
        JOIN subjects ON subjects.subject_id = courses.subject_id
        WHERE subjects.subject_id = LOWER($1) AND courses.course_code = LOWER($2)`, 
        [subject, code]);

    if (getCourseResult.rowCount === 0) {
        return res.json({ success: false, msg: `Cannot found course ${subject.toUpperCase()}-${code.toUpperCase()}`});
    }
    
    const courseId = getCourseResult.rows[0].course_id;
    
    // insert into starred courses table
    await client.query(`DELETE FROM starred_courses
        WHERE student_id = $1 AND course_id = $2`, [studentId, courseId]);

    return res.json({ success: true });
});