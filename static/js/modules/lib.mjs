export const forEach = function (array, callback, scope) {
    for (var i = 0; i < array.length; i++) {
        callback.call(scope, i, array[i]); // passes back stuff we need
    }
};

export const pause = async function (time) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve();
        }, time);
    })
}

/* words like 'a', 'and, 'the' would not be capitalized unless it is at start of string */
export function capitalizeTitleWithConvention(string) {
    let words = string.split(' ');
    const exceptions = ['a', 'an', 'the', 'and', 'to', 'on', 'of', 'for'];
    
    words = words.map((word, index) => {
        if (index === 0) return capitalizeWord(word);

        if (exceptions.some(exception => word === exception) && !words[index - 1].match(/:$/)) return word;

        return capitalizeWord(word);
    })

    return words.join(' ');
}

export function capitalizeWord(word) {
    const allUpperExceptions = ['ii', 'iii', 'iv', 'vi', 'vii', 'viii', 'ix', 'x', 'm&a'];

    if (typeof word !== 'string') return '';

    if (allUpperExceptions.some(exception => word === exception)) return word.toUpperCase();
    return word[0].toUpperCase() + word.slice(1);
}

export class Course {
    constructor(course) {
        this.id = course.id;
        this.subject = course.subject;
        this.code = course.code;
        this.title = course.title;
        this.credit = course.credit;

        /*
        Below are obj that has values of Activity[]
        E.g.
        labs: {
            LA1: [
                {type: 2, name: "LA1", date: "Thu"...}
                {type: 2, name: "LA1", date: "Fri"...}
            ],
            LA2: ...
        }
        */
        this.lectures = course.lectures;
        this.tutorials = course.tutorials;
        this.labs = course.labs;
        this.researches = course.researches;
        this.commonCoreArea = course.commonCoreArea;
    }

    getLectureTimeSlots() {
        const data = [];
        const TimeSlots = '0123456789abcdefghijklmnopqrsx';

        for (let key in this.lectures) {
            const lecture = this.lectures[key];

            const sessionIDs = [];
            const weekdays = [];
            const timeSlots = [];
            
            for (let section of lecture) {
                sessionIDs.push(section.sessionID);

                if (!section.date) {
                    weekdays.push(5);
                } else {
                    weekdays.push(Weekdays[section.date]);
                }

                const startTimeMarker = this.calculateTimeMarker(section.startTime);
                const endTimeMarker = this.calculateTimeMarker(section.endTime);
                let timeSlot = TimeSlots.slice(startTimeMarker, endTimeMarker)
                if (timeSlot === '') timeSlot = 'x';
                timeSlots.push(timeSlot);
            }

            data.push({ sessionIDs: sessionIDs, weekdays: weekdays, timeSlots: timeSlots });
        }

        return data;
    }

    getTutorialTimeSlots() {
        const data = [];
        const TimeSlots = '0123456789abcdefghijklmnopqrsx';

        for (let key in this.tutorials) {
            const tutorial = this.tutorials[key];

            const sessionIDs = [];
            const weekdays = [];
            const timeSlots = [];
            
            for (let section of tutorial) {
                sessionIDs.push(section.sessionID);

                if (!section.date) {
                    weekdays.push(5);
                } else {
                    weekdays.push(Weekdays[section.date]);
                }

                const startTimeMarker = this.calculateTimeMarker(section.startTime);
                const endTimeMarker = this.calculateTimeMarker(section.endTime);
                let timeSlot = TimeSlots.slice(startTimeMarker, endTimeMarker)
                if (timeSlot === '') timeSlot = 'x';
                timeSlots.push(timeSlot);
            }

            data.push({ sessionIDs: sessionIDs, weekdays: weekdays, timeSlots: timeSlots });
        }

        return data;
    }

    getLabTimeSlots() {
        const data = [];
        const TimeSlots = '0123456789abcdefghijklmnopqrsx';

        for (let key in this.labs) {
            const lab = this.labs[key];

            const sessionIDs = [];
            const weekdays = [];
            const timeSlots = [];
            
            for (let section of lab) {
                sessionIDs.push(section.sessionID);

                if (!section.date) {
                    weekdays.push(5);
                } else {
                    weekdays.push(Weekdays[section.date]);
                }

                const startTimeMarker = this.calculateTimeMarker(section.startTime);
                const endTimeMarker = this.calculateTimeMarker(section.endTime);
                let timeSlot = TimeSlots.slice(startTimeMarker, endTimeMarker)
                if (timeSlot === '') timeSlot = 'x';
                timeSlots.push(timeSlot);
            }

            data.push({ sessionIDs: sessionIDs, weekdays: weekdays, timeSlots: timeSlots });
        }

        return data;
    }

    getResearchTimeSlots() {
        const data = [];
        const TimeSlots = '0123456789abcdefghijklmnopqrstuvx';

        for (let key in this.researches) {
            const research = this.researches[key];

            const sessionIDs = [];
            const weekdays = [];
            const timeSlots = [];
            
            for (let section of research) {
                sessionIDs.push(section.sessionID);

                if (!section.date) {
                    weekdays.push(5);
                } else {
                    weekdays.push(Weekdays[section.date]);
                }

                const startTimeMarker = this.calculateTimeMarker(section.startTime);
                const endTimeMarker = this.calculateTimeMarker(section.endTime);
                let timeSlot = TimeSlots.slice(startTimeMarker, endTimeMarker)
                if (timeSlot === '') timeSlot = 'x';
                timeSlots.push(timeSlot);
            }

            data.push({ sessionIDs: sessionIDs, weekdays: weekdays, timeSlots: timeSlots });
        }

        return data;
    }

    getTimeSlots() {
        const data = {};
        data.courseID = this.id;
        data.courseCode = (this.subject + this.code).toUpperCase();
        data.lectures = this.getLectureTimeSlots();
        data.tutorials = this.getTutorialTimeSlots();
        data.labs = this.getLabTimeSlots();
        data.researches = this.getResearchTimeSlots();

        return data;
    }
    
    /* 08:00 = 1, 08:30 = 2, 09:00 = 3, ..., 23:00 = 30 */
    calculateTimeMarker(time) {
        if (time === null) return -1;
        const times = time.split(':');
        const hour = parseInt(times[0]);
        const minute = parseInt(times[1]);

        return Math.round(((hour * 60 + minute) / 30) - 16);
    }

    getLectureData() {
        const data = [];

        for (let lecture in this.lectures) {
            const datum = {
                courseCode: (this.subject + this.code).toUpperCase(),
                fullName: this.getActivityFullName(lecture, ActivityType.Lecture),
                dates: this.getActivityDates(this.lectures[lecture]),
                time: this.getActivityTime((this.lectures[lecture])[0]),
                venue: this.getActivityVenue((this.lectures[lecture])[0]),
                instructor: this.getInstructorName((this.lectures[lecture])[0]),
                sessionID: this.getActivitySessionID((this.lectures[lecture])[0])
            }

            data.push(datum);
        }

        return data;
    }

    getTutorialData() {
        const data = [];

        for (let tutorial in this.tutorials) {
            const datum = {
                courseCode: (this.subject + this.code).toUpperCase(),
                fullName: this.getActivityFullName(tutorial, ActivityType.Tutorial),
                dates: this.getActivityDates(this.tutorials[tutorial]),
                time: this.getActivityTime((this.tutorials[tutorial])[0]),
                venue: this.getActivityVenue((this.tutorials[tutorial])[0]),
                instructor: this.getInstructorName((this.tutorials[tutorial])[0])
            }

            data.push(datum);
        }

        return data;
    }

    getLabData() {
        const data = [];

        for (let lab in this.labs) {
            const datum = {
                courseCode: (this.subject + this.code).toUpperCase(),
                fullName: this.getActivityFullName(lab, ActivityType.Lab),
                dates: this.getActivityDates(this.labs[lab]),
                time: this.getActivityTime((this.labs[lab])[0]),
                venue: this.getActivityVenue((this.labs[lab])[0]),
                instructor: this.getInstructorName((this.labs[lab])[0])
            }

            data.push(datum);
        }

        return data;
    }

    getResearchData() {
        const data = [];

        for (let research in this.researches) {
            const datum = {
                courseCode: (this.subject + this.code).toUpperCase(),
                fullName: this.getActivityFullName(research, ActivityType.Research),
                dates: this.getActivityDates(this.researches[research]),
                time: this.getActivityTime((this.researches[research])[0]),
                venue: this.getActivityVenue((this.researches[research])[0]),
                instructor: this.getInstructorName((this.researches[research])[0])
            }

            data.push(datum);
        }

        return data;
    }

    getActivityData() {
        let data = [];

        data = data.concat(this.getLectureData());
        data = data.concat(this.getTutorialData());
        data = data.concat(this.getLabData());
        data = data.concat(this.getResearchData());

        return data;
    }

    /* e.g. LA1 => Lab 1, L01 -> Lecture 1, T1 => Tutorial */
    getActivityFullName(activityName, type) {
        let regEXPrefix = '';

        switch (type) {
            case ActivityType.Lecture:
                regEXPrefix = 'L';
                break;

            case ActivityType.Tutorial:
                regEXPrefix = 'T';
                break;
            
            case ActivityType.Lab:
                regEXPrefix = 'LA';
                break;
            
            case ActivityType.Research:
                regEXPrefix = 'R';
                break;
            
            default:
                regEXPrefix = ' ';
        }

        const regexExpression = new RegExp ('^' + regEXPrefix + '0*(.*)$');
        const numbersMatchedArray = activityName.match(regexExpression);

        let numbers = '';
        if (numbersMatchedArray) {
            numbers = ' ' + numbersMatchedArray[1];
        } else {
            numbers = '';
        }

        return ActivityType[type] + numbers;
    }

    getActivityDates(activities) {
        let result = activities.reduce((result, activity) => {
            if (!activity.date) return result;
            return (result + activity.date + ', ')
        }, '')

        /* cut the excess ', ' at the end of string */
        result = result.slice(0, -2);

        if (result === '') result = "TBA";

        return result;
    }

    getActivityTime(activity) {
        if (!activity.startTime) return "TBA";

        return activity.startTime.slice(0, -3) + '-' + activity.endTime.slice(0, -3);
    }

    getActivityVenue(activity) {
        if (!activity.venue) return "TBA";

        return activity.venue;
    }

    getInstructorName(activity) {
        if (!activity.instructorFirstName) return "TBA";

        return activity.instructorFirstName + ', ' + activity.instructorLastName;
    }

    getActivitySessionID(activity) {
        return activity.sessionID;
    }

    getCommonCoreArea() {
        return this.commonCoreArea;
    }
}

/* enum-equivalent in JS */
export let ActivityType;
(function (ActivityType) {
    ActivityType[ActivityType["Lecture"] = 0] = "Lecture";
    ActivityType[ActivityType["Tutorial"] = 1] = "Tutorial";
    ActivityType[ActivityType["Lab"] = 2] = "Lab";
    ActivityType[ActivityType["Research"] = 3] = "Research";
})(ActivityType || (ActivityType = {}));

/* weekday enum */
export let Weekdays;
(function (Weekdays) {
    Weekdays[Weekdays["Mon"] = 0] = "Mon";
    Weekdays[Weekdays["Tue"] = 1] = "Tue";
    Weekdays[Weekdays["Wed"] = 2] = "Wed";
    Weekdays[Weekdays["Thu"] = 3] = "Thu";
    Weekdays[Weekdays["Fri"] = 4] = "Fri";
})(Weekdays || (Weekdays = {}));

export class Activity {
    constructor(activity) {
        this.type = activity.type;
        this.name = activity.name;
        this.date = activity.date;
        this.startTime = activity.start_time;
        this.endTime = activity.end_time;
        this.venue = activity.venue;
        this.instructorFirstName = activity.instructorFirstName;
        this.instructorLastName = activity.instructorLastName;
        this.sessionID = activity.sessionID;
    }
}