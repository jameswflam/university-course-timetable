import { forEach, capitalizeTitleWithConvention } from './lib.mjs';

export async function unboundPopulateSchools() {
    const res = await fetch('/schools', {
        method: "GET"
    })

    const result = await res.json();

    for (let school of result.schools) {
        const schoolName = school.name[0].toUpperCase() + school.name.slice(1);

        if (this.schools.some(filter => filter === schoolName)) {
            createOption(schoolName, 'schools', true);
        } else {
            createOption(schoolName, 'schools');
        }
    }
}

export async function unboundPopulateSubjects(schoolFilters = [], commonCoreFilter = false) {
    let queryString = schoolFilters.reduce((queryString, filter, index) => {
        return queryString + (index === 0 ? '' : '&').concat(`schoolFilter=${filter}`);
    }, '?');

    if (commonCoreFilter) queryString = queryString + (queryString === '?' ? '' : '&').concat(`commonCoreFilter=${commonCoreFilter}`);

    queryString = '/subjects' + queryString;

    const res = await fetch(queryString, {
        method: "GET"
    });

    const result = await res.json();

    const optionHeros = document.querySelectorAll('#subject-options .option-hero');
    forEach(optionHeros, (index, hero) => {
        hero.remove();
    });

    for (let subject of result.subjects) {
        if (this.subjects.some(filter => filter === subject.id.toUpperCase())) {
            createOption(subject.id.toUpperCase(), 'subjects', true);
        } else {
            createOption(subject.id.toUpperCase(), 'subjects');
        }
    }
}

export function createOption(content, type, selected) {
    const newOptionHero = document.createElement('div');
    newOptionHero.classList.add('option-hero');
    newOptionHero.setAttribute('data-type', type);
    newOptionHero.setAttribute('content', content);

    const newOption = document.createElement('div');
    newOption.classList.add('option');
    newOption.classList.add('no-select');
    newOption.innerText = capitalizeTitleWithConvention(content);
    newOptionHero.appendChild(newOption);

    const newSVG = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
    newSVG.setAttribute('version', '1.1');
    newSVG.classList.add('tick');
    newSVG.setAttribute('xmlns', 'http://www.w3.org/2000/svg');
    newSVG.setAttribute('xmlns:xlink', 'http://www.w3.org/1999/xlink');
    newSVG.setAttribute('x', '0px');
    newSVG.setAttribute('y', '0px');
    newSVG.setAttribute('viewBox', '0 0 21 16');
    newSVG.setAttribute('xml:space', 'preserve');
    newOptionHero.appendChild(newSVG);

    const newPolyline = document.createElementNS('http://www.w3.org/2000/svg', 'polyline');
    newPolyline.classList.add('tick-path');
    if (selected) newPolyline.classList.add('drawn');
    newPolyline.setAttribute('points', '3,9 7,13 18,3');
    newSVG.appendChild(newPolyline);

    let container;
    if (type === 'schools') {
        container = document.querySelector('#school-options .multi-selector-options')
    } else if (type === 'subjects') {
        container = document.querySelector('#subject-options .multi-selector-options')
    }

    container.insertBefore(newOptionHero, container.querySelector('.end-of-list'));
}

export function createBadge(content, type, contentAttribute) {
    let colorClass = '';

    switch (type) {
        case "schools":
            colorClass = "school-badge";
            break;

        case "subjects":
            colorClass = "subject-badge";
            break;

        case "areas":
            colorClass = "area-badge";
            break;

        case "commonCores":
            colorClass = "common-core-badge";
            break;

        default:
            colorClass = "school-badge";
    }

    const newBadge = document.createElement('div');
    newBadge.classList.add('filter-badge');
    newBadge.classList.add(colorClass);
    newBadge.classList.add('no-select');
    newBadge.setAttribute('content', contentAttribute);

    const newDiv = document.createElement('div');
    newBadge.appendChild(newDiv);

    const newName = document.createElement('div');
    newName.classList.add('filter-name');
    newName.innerHTML = content;
    newDiv.appendChild(newName);

    const newClose = document.createElement('div');
    newClose.classList.add('filter-close');
    newDiv.appendChild(newClose);

    const newI = document.createElement('i');
    newClose.classList.add('bi');
    newClose.classList.add('bi-x');
    newClose.appendChild(newI);

    document.querySelector('#filter-badges-container').insertBefore(newBadge, document.querySelector('#end-of-badges'));
}

export function deleteBadgeByContent(content) {
    const badges = document.querySelectorAll('.filter-badge');
    if (!badges) return false;

    forEach(badges, (index, badge) => {
        if (badge.querySelector('.filter-name').innerText === content) {
            badge.remove();
        }
    });
}