import { forEach } from './lib.mjs';

export function optionBtnClicked(event) {
    const btn = getParentElementByClass(event.target, 'option-hero');
    if (!btn) return;

    let content = btn.getAttribute('content');
    const type = btn.getAttribute('data-type');

    let filters = this[type];
    let action;
    if (filters.some(filter => filter === content)) {
        action = 'delete';
    } else {
        action = 'add';
    }

    const filterChanged = new CustomEvent('filterChanged', {
        bubbles: true,
        detail: {
            type: type,
            content: content,
            action: action,
            btn: btn
        }
    });

    document.dispatchEvent(filterChanged);
}

export function badgeCloseBtnClicked(event) {
    /* make sure close button on badge is clicked */
    if (!event.target.classList.contains('filter-close')) return;

    const badge = event.target.parentElement.parentElement;

    let type;
    if (badge.classList.contains('school-badge')) type = 'schools';
    if (badge.classList.contains('subject-badge')) type = 'subjects';
    if (badge.classList.contains('area-badge')) type = 'areas';
    if (badge.classList.contains('common-core-badge')) type = 'commonCores';

    let content;
    let btn;
    
    if (type === 'schools' || type === 'subjects' || type === 'areas') {
        content = badge.getAttribute('content');
        btn = selectBtnByContent(badge.querySelector('.filter-name').innerText);
    } else if (type === 'commonCores') {
        content = undefined;
        btn = selectCommonCoreToggleBtn();
    }

    const action = 'delete';

    const filterChanged = new CustomEvent('filterChanged', {
        bubbles: true,
        detail: {
            type: type,
            content: content,
            action: action,
            btn: btn
        }
    });

    document.dispatchEvent(filterChanged);
}

export function commonCoreToggleClicked(event) {
    const btn = event.currentTarget;
    const action = getCommonCoreBtnAction(btn);

    const filterChanged = new CustomEvent('filterChanged', {
        bubbles: true,
        detail: {
            type: 'commonCores',
            content: undefined,
            action: action,
            btn: btn
        }
    });

    document.dispatchEvent(filterChanged);
}

function selectCommonCoreToggleBtn() {
    return document.querySelector('#common-core-filter-toggle');
}

function getCommonCoreBtnAction(btn) {
    let action = '';

    if (btn.classList.contains('btn-outline-danger')) {
        action = 'add';
    } else {
        action = 'delete';
    }

    return action;
}

export function toggleCommonCoreBtnAppearance() {
    const btn = document.querySelector('#common-core-filter-toggle');
    if (!btn) return;

    const checkbox = document.querySelector('#common-core-filter-checkbox');
    checkbox.checked = !checkbox.checked;

    let action = ''

    if (btn.classList.contains('btn-outline-danger')) {
        activateCommonCoreBtn();
    } else {
        deactivateCommonCoreBtn();
    }

    return action;
}

export function activateCommonCoreBtn() {
    const btn = document.querySelector('#common-core-filter-toggle');
    if (!btn) return;

    const checkbox = document.querySelector('#common-core-filter-checkbox');
    checkbox.checked = true;

    btn.classList.add('btn-danger');
    btn.classList.remove('btn-outline-danger');

    return;
}

export function deactivateCommonCoreBtn() {
    const btn = document.querySelector('#common-core-filter-toggle');
    if (!btn) return;

    const checkbox = document.querySelector('#common-core-filter-checkbox');
    checkbox.checked = false;

    btn.classList.remove('btn-danger');
    btn.classList.add('btn-outline-danger');

    return;
}

function selectBtnByContent(content) {
    const btns = document.querySelectorAll('.option-hero');
    let result = null;

    forEach(btns, (index, btn) => {
        if (btn.querySelector('.option').innerText === content) {
            result = btn;
        }
    })

    return result;
}

export function getParentElementByClass(elem, checkClass) {
    if (elem === null) {
        return null;
    }

    if (elem.tagName === "HTML") {
        return null;
    }

    if (elem.classList.contains(checkClass)) {
        return elem;
    } else {
        return getParentElementByClass(elem.parentElement, checkClass);
    }
}