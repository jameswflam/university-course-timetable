import { createHTMLElement, Attribute, assertHTMLElement, courseCardType } from './modules/createHTMLElement.mjs';
import { capitalizeWord, Course } from './modules/lib.mjs';

let nextCardId = 0;
let nextTimeRow = 0;

const refineModal = new bootstrap.Modal(document.querySelector('#refine-modal'));
const structureModal = new bootstrap.Modal(document.querySelector('#add-course-modal'));

const toastNoResult = document.getElementById('toastNoResult');
const toastResult = document.getElementById('toastResult');

const configurations = {
    credits: '',
    mustTake: {},
    mustTakeArea: {},
    priorities: {},
    timeOff: {}
}

window.onload = () => {
    AOS.init();
}

document.querySelector('#credits').addEventListener('focusout', (event) => {
    const input = event.target;
    const value = input.value;

    const configurationsChanged = new CustomEvent('configurationsChanged', {
        detail: {
            action: 'add',
            type: 'credits',
            content: value
        }
    })

    document.dispatchEvent(configurationsChanged);
})

document.addEventListener('configurationsChanged', (event) => {
    const action = event.detail.action;
    const type = event.detail.type;
    const content = event.detail.content;
    const cardId = event.detail.cardId;

    if (action === 'add') {
        if (type === 'commonCore') {
            configurations.mustTakeArea[cardId] = content;

            if (!configurations.priorities[cardId]) {
                configurations.priorities[cardId] = [];
            }
        }

        if (type === 'general') {
            configurations.mustTake[cardId] = content;
        }

        if (type === 'refine') {
            if (!configurations.priorities[cardId]) {
                configurations.priorities[cardId] = [];
            }

            configurations.priorities[cardId].push(content);
        }

        if (type === 'credits') {
            configurations.credits = content;
        }

        if (type === 'time') {
            if (!configurations.timeOff[cardId]) configurations.timeOff[cardId] = {};
        }

        if (type === 'time-week') {
            if (!configurations.timeOff[cardId]) configurations.timeOff[cardId] = {};
            configurations.timeOff[cardId].weekday = content;
        }

        if (type === 'time-startTime') {
            if (!configurations.timeOff[cardId]) configurations.timeOff[cardId] = {};
            configurations.timeOff[cardId].startTime = content;
        }

        if (type === 'time-endTime') {
            if (!configurations.timeOff[cardId]) configurations.timeOff[cardId] = {};

            configurations.timeOff[cardId].endTime = content;
        }
    }

    if (action === 'delete') {

        if (type === 'commonCore') {
            if (configurations.mustTakeArea[cardId]) delete configurations.mustTakeArea[cardId];

            if (configurations.priorities[cardId]) {
                delete configurations.priorities[cardId];
            }
        }

        if (type === 'general') {
            if (configurations.mustTake[cardId]) delete configurations.mustTake[cardId];
        }

        if (type === 'refine') {
            if (configurations.priorities[cardId]) {
                configurations.priorities[cardId] = configurations.priorities[cardId].filter(course => course !== content);
            }
        }

        if (type === 'time') {
            delete configurations.timeOff[cardId];
        }
    }

    console.log(configurations);
})

document.addEventListener('addCourseToStructure', (event) => {
    const endOfList = document.querySelector('#structure-section-hero .plus-sign');
    const type = event.detail.type;
    const area = event.detail.area;
    const content = event.detail.content;

    const newCard = createCourseCard(type, area, content);
    const newId = 'card' + nextCardId;
    newCard.id = newId;
    endOfList.insertAdjacentElement('beforebegin', newCard);

    /* insert row in refine section */
    if (type === 'commonCore') {
        const newCard2 = createCourseCard(type, area, content);
        newCard2.setAttribute('target-card', newCard.id);
        newCard2.setAttribute('card-type', 'title');

        const newRow = createRowToRefine(newCard2, newCard.id, area);
        document.querySelector('#refine-section-hero .col-lg-8').insertAdjacentElement('beforeend', newRow);
    }

    const configurationsChanged = new CustomEvent('configurationsChanged', {
        detail: {
            action: 'add',
            type: type,
            content: type === 'general' ? content : area,
            cardId: newId
        }
    })
    document.dispatchEvent(configurationsChanged);

    nextCardId++;
})

/* add course card to structure section */
const badgeContainers = document.querySelectorAll('#add-course-modal .badge-container');
for (let i = 0; i < badgeContainers.length; i++) {
    badgeContainers[i].addEventListener('click', (event) => {
        const btn = event.target;
        if (!event.target.classList.contains('modal-badge')) return;

        let content = '';

        /* get display content and course card type in the new card */
        let type = btn.getAttribute('card-type');
        let area = btn.getAttribute('cc-area');
        if (type === 'commonCore') {  // common core badge
            if (area === 'hlth' | area === 'sa') {
                content = area.toUpperCase();
            } else if (area === 'sat') {
                content = 's&t';
            } else {
                content = capitalizeWord(area);
            }
        } else {        // general badge
            content = btn.innerText;
        }

        const addCourse = new CustomEvent('addCourseToStructure', {
            bubbles: true,
            detail: {
                type: type,
                area: area,
                content: content,
            }
        })

        document.dispatchEvent(addCourse);
    })
}

document.querySelector('#structure-section-hero .col-lg-8').addEventListener('click', async (event) => {
    /* delete course card from structure section */
    if (event.target.classList.contains('bi-x')) {
        const targetCard = event.target.parentElement;
        const id = targetCard.id;

        if (targetCard) targetCard.remove();

        let type = '';
        /* delete row in refine section if the targetCard is a common core card */
        const targetRow = document.querySelector(`.refine-flex-container[target-card="${id}"]`);
        if (targetRow) {
            targetRow.remove();

            type = 'commonCore';
        } else {
            type = 'general';
        }

        const configurationsChanged = new CustomEvent('configurationsChanged', {
            detail: {
                action: 'delete',
                type: type,
                content: '',
                cardId: id
            }
        })
        document.dispatchEvent(configurationsChanged);
    }

    /* add course card to structure section */
    if (event.target.classList.contains('bi-plus-circle')) {
        const res = await fetch('/starredCourses');
        const favCourses = (await res.json()).courses;

        const target = document.querySelector('#add-course-modal .badge-container');

        /* clear old badges */
        const oldBadges = target.querySelectorAll('*');
        for (let i = 0; i < oldBadges.length; i++) {
            oldBadges[i].remove();
        }

        /* insert new badges */
        for (let course of favCourses) {
            const courseFullCode = (course.subject + ' ' + course.code).toUpperCase();
            const commonCoreArea = course.commonCoreArea === 's&t' ? 'sat' : course.commonCoreArea;

            let skip = false;
            for (let key in configurations.mustTake) {
                if (configurations.mustTake[key] === courseFullCode) {
                    skip = true;
                }
            }
            for (let key in configurations.priorities) {
                if (configurations.priorities[key].some(course => course === courseFullCode)) skip = true;
            }
            if (skip) continue;

            if (commonCoreArea === null) {
                const courseBadge = createCourseBadge(courseFullCode, 'general', courseFullCode);
                target.insertAdjacentElement('beforeend', courseBadge);
            } else {
                const courseBadge = createCourseBadge(courseFullCode, commonCoreArea, courseFullCode);
                target.insertAdjacentElement('beforeend', courseBadge);
            }
        }

        /* launch the modal */
        structureModal.show();
    }
});

document.querySelector('#refine-section-hero .col-lg-8').addEventListener('click', async (event) => {
    if (event.target.classList.contains('bi-x')) {
        const card = event.target.parentElement;
        const id = card.getAttribute('target-card');
        const cardCourseCode = card.querySelector('.course-info').innerText;
        let type = ''

        /* delete whole row from refine section */
        if (card.getAttribute('card-type') === 'title') {
            const targetRow = document.querySelector(`.refine-flex-container[target-card="${id}"]`);
            if (targetRow) targetRow.remove();

            const targetCard = document.querySelector(`#${id}`);
            if (targetCard) targetCard.remove();

            type = 'commonCore';
        } else {
            /* delete course card from refine flex container */
            card.previousSibling.remove();
            card.remove();

            type = 'refine'
        }

        const configurationsChanged = new CustomEvent('configurationsChanged', {
            detail: {
                action: 'delete',
                type: type,
                content: cardCourseCode,
                cardId: id
            }
        })
        document.dispatchEvent(configurationsChanged);
    }

    /* add course card to refine section */
    if (event.target.classList.contains('bi-plus-circle')) {

        const btn = event.target;
        let ccArea = btn.getAttribute('cc-area');
        ccArea = ccArea === 'sat' ? 's%26t' : ccArea;
        const id = btn.getAttribute('target-card');

        /* query database for list of common cores in specified area */
        const res = await fetch(`/common-cores?areaFilter=${ccArea}`, {
            method: 'GET'
        })
        const result = await res.json();

        /* populate the select menu in the modal */
        modifyRefineModalSelect(result.courses, id, btn.getAttribute('cc-area'));

        /* populate favourite course section */
        const res2 = await fetch(`/starredCourses?areaFilter=${ccArea}`);
        const favCourses = (await res2.json()).courses;

        const target = document.querySelector('#refine-modal .badge-container');

        /* clear old badges */
        const oldBadges = target.querySelectorAll('*');
        for (let i = 0; i < oldBadges.length; i++) {
            oldBadges[i].remove();
        }

        /* insert new badges */
        for (let course of favCourses) {
            const courseFullCode = (course.subject + ' ' + course.code).toUpperCase();
            const commonCoreArea = course.commonCoreArea === 's&t' ? 'sat' : course.commonCoreArea;

            let skip = false
            for (let key in configurations.mustTake) {
                if (configurations.mustTake[key] === courseFullCode) {
                    skip = true;
                }
            }
            if (configurations.priorities[id].some(course => course === courseFullCode)) skip = true;
            if (skip) continue;

            if (commonCoreArea === null) {
                const courseBadge = createCourseBadge(courseFullCode, 'general', id);
                target.insertAdjacentElement('beforeend', courseBadge);
            } else {
                const courseBadge = createCourseBadge(courseFullCode, commonCoreArea, id);
                target.insertAdjacentElement('beforeend', courseBadge);
            }
        }

        /* launch the modal */
        refineModal.show();
    }

});

/* add course card to refine section */
document.querySelector('#refine-modal .dropdown').addEventListener('click', (event) => {
    if (!event.target.classList.contains('dropdown-item')) return;

    const btn = event.target;
    const type = btn.getAttribute('cc-area');
    const targetId = btn.getAttribute('target-card');

    const rightArrow = createRightArrow();
    const newCard = createFadedCourseCard(type, btn.innerText);
    newCard.setAttribute('target-card', targetId);

    const target = document.querySelector(`.refine-flex-container[target-card="${targetId}"] .common-core-list .plus-sign`);
    target.insertAdjacentElement('beforebegin', rightArrow);
    target.insertAdjacentElement('beforebegin', newCard);

    const configurationsChanged = new CustomEvent('configurationsChanged', {
        detail: {
            action: 'add',
            type: 'refine',
            content: btn.innerText,
            cardId: targetId
        }
    })
    document.dispatchEvent(configurationsChanged);
});

document.querySelector('#refine-modal #fav-area').addEventListener('click', (event) => {
    if (!event.target.classList.contains('modal-badge')) return;

    const btn = event.target;
    const type = btn.getAttribute('cc-area');
    const targetId = btn.getAttribute('target-card');

    const rightArrow = createRightArrow();
    const newCard = createFadedCourseCard(type, btn.innerText);
    newCard.setAttribute('target-card', targetId);

    const target = document.querySelector(`.refine-flex-container[target-card="${targetId}"] .common-core-list .plus-sign`);
    target.insertAdjacentElement('beforebegin', rightArrow);
    target.insertAdjacentElement('beforebegin', newCard);

    const configurationsChanged = new CustomEvent('configurationsChanged', {
        detail: {
            action: 'add',
            type: 'refine',
            content: btn.innerText,
            cardId: targetId
        }
    })
    document.dispatchEvent(configurationsChanged);
});

/* add time row */
document.querySelector('#time-off-hero .bi-plus-circle').addEventListener('click', (event) => {
    const newTimeRow = createTimeRow();

    const target = event.target.parentElement.parentElement;
    target.insertAdjacentElement('beforebegin', newTimeRow);

    const configurationsChanged = new CustomEvent('configurationsChanged', {
        detail: {
            action: 'add',
            type: 'time',
            cardId: newTimeRow.id
        }
    })
    document.dispatchEvent(configurationsChanged);
});


document.querySelector('#time-off-hero .col-lg-8').addEventListener('click', (event) => {
    /* delete time row */
    if (event.target.classList.contains('bi-x')) {
        const closeBtn = event.target;
        const targetId = closeBtn.getAttribute('target-time-row');

        const targetRow = document.querySelector(`#${targetId}`);
        targetRow.remove();

        const configurationsChanged = new CustomEvent('configurationsChanged', {
            detail: {
                action: 'delete',
                type: 'time',
                cardId: targetId
            }
        })
        document.dispatchEvent(configurationsChanged);
    }

    /* weekday is changed */
    if (event.target.classList.contains('dropdown-item')) {
        const btn = event.target;
        const targetRowId = btn.getAttribute('target-time-row');
        let content = btn.getAttribute('weekday-code');

        const targetRow = document.querySelector(`#${targetRowId}`);
        const valueContainer = targetRow.querySelector('.weekdays-button text');
        valueContainer.innerText = btn.innerText;

        const configurationsChanged = new CustomEvent('configurationsChanged', {
            detail: {
                action: 'add',
                type: 'time-week',
                content: content,
                cardId: targetRowId
            }
        })
        document.dispatchEvent(configurationsChanged);
    }
});


document.querySelector('#time-off-hero .col-lg-8').addEventListener('focusout', (event) => {
    let type;
    if (event.target.getAttribute('name') === 'start-time') {
        type = 'time-startTime';
    } else if (event.target.getAttribute('name') === 'end-time') {
        type = 'time-endTime';
    } else {
        return;
    }

    const input = event.target;
    const targetRowId = input.getAttribute('target-time-row');
    let content = input.value;


    if (type === 'time-endTime') {
        if (configurations.timeOff[targetRowId] && (content <= configurations.timeOff[targetRowId].startTime)) {
            alert('End time cannot be earlier than start time!');
            input.value = '';
            content = '';
        }
    }

    if (type === 'time-startTime') {
        if (configurations.timeOff[targetRowId] && (content >= configurations.timeOff[targetRowId].endTime)) {
            alert('Start time cannot be later than start time!');
            input.value = '';
            content = '';
        }
    }

    const configurationsChanged = new CustomEvent('configurationsChanged', {
        detail: {
            action: 'add',
            type: type,
            content: content,
            cardId: targetRowId
        }
    })
    document.dispatchEvent(configurationsChanged);
});

// const configurations = {
//     credits: '',
//     mustTake: {},
//     mustTakeArea: {},
//     priorities: {},
//     timeOff: {}
// }

document.querySelector('#generateBtn').addEventListener('click', async (event) => {
    if (configurations.credits === '0' || configurations.credits === '') return alert('Credits cannot be 0.');

    if (!Object.keys(configurations.mustTake).length && !Object.keys(configurations.mustTakeArea).length) {
        return alert('Please input your study plan.');
    }

    for (let key in configurations.timeOff) {
        const timeOff = configurations.timeOff[key];
        if (!timeOff.weekday || !timeOff.startTime || !timeOff.endTime) {
            return alert('Please complete the time off section.');
        }
    }

    /* make the path for AJAX request */
    const query = new URLSearchParams();
    const mustTakes = configurations.mustTake
    for (let key in mustTakes) {
        query.append('mustTake', mustTakes[key])
    }
    const path = '/mustTakes?' + query.toString();

    /* GET /mustTakes query */
    const res = await fetch(path, {
        method: "GET",
    });

    const result = await res.json();
    if (!result.success) {
        console.log(result.msg);
    }

    /* generate all the possible timetables */
    let timetables = [['', '', '', '', '', '']];
    let sessionIDs = [[]];

    if (result.success) {
        /* convert results to Course objects */
        const mustTakeCourses = result.courses
        const mustTakeCourseObjs = [];
        for (let course of mustTakeCourses) {
            mustTakeCourseObjs.push(new Course(course));
        }
        
        for (let i = 0; i < mustTakeCourseObjs.length; i++) {
            const course = mustTakeCourseObjs[i];
            const courseTimeSlots = course.getTimeSlots();
    
            const addResult = addCourseToTimetables(timetables, sessionIDs, courseTimeSlots);
            timetables = addResult.timetables;
            sessionIDs = addResult.sessionIDs;
        }
    }

    /* get priorities courses */
    const priorities = {};
    for (let key in configurations.mustTakeArea) {
        const ccPriorities = configurations.priorities[key];
        priorities[key] = [];

        for (let course of ccPriorities) {
            const subject = (course.split(' '))[0];
            const code = (course.split(' '))[1];

            const query = new URLSearchParams();
            query.append('subjectFilter', subject);
            query.append('codeFilter', code)
            const path = '/common-cores?' + query.toString();

            /* GET /common-cores query */
            const res = await fetch(path, {
                method: "GET",
            });
            const result = await res.json();

            priorities[key].push(new Course(result.courses[0]).getTimeSlots());
        }
    }

    let timetablesWithPriorities = {
        0: Object.assign([], timetables)
    };
    let sessionIDsWithPriorities = {
        0: Object.assign([], sessionIDs)
    };

    for (let card in priorities) {
        const coursePriorities = priorities[card];
        let workingTimetable = {};
        let workingSessionID = {};

        for (let key in timetablesWithPriorities) {
            if (!timetablesWithPriorities[key]) continue;
            for (let i = 0; i < coursePriorities.length; i++) {
                const courseTimeSlots = coursePriorities[i];

                const addResult = addCourseToTimetables(timetablesWithPriorities[key], sessionIDsWithPriorities[key], courseTimeSlots);
                if (!addResult.timetables.length) continue;

                const priority = parseInt(key) + i;

                workingTimetable[priority] = addResult.timetables;
                workingSessionID[priority] = addResult.sessionIDs;
            }

            timetablesWithPriorities = Object.assign({}, workingTimetable);
            sessionIDsWithPriorities = Object.assign({}, workingSessionID);
        }
    }

    console.log(timetablesWithPriorities);
    console.log(sessionIDsWithPriorities);

    let prioritySuccess = false;
    for (let key in timetablesWithPriorities) {
        if (timetablesWithPriorities[key]) {
            timetables = timetablesWithPriorities[key];
            sessionIDs = sessionIDsWithPriorities[key];
            prioritySuccess = true;
            break;
        }
    }
    if (!prioritySuccess) {
        timetables = [];
        sessionIDs = [];
    }

    /* delete timetables that conflicts time off */
    if (timetables.length) {
        for (let key in configurations.timeOff) {
            const TimeSlots = '0123456789abcdefghijklmnopqrstuvwxyz';
            const timeOff = configurations.timeOff[key];
    
            const weekday = parseInt(timeOff.weekday);
            const startTime = timeOff.startTime < "08:00" ? "08:00" : timeOff.startTime;
            const endTime = timeOff.endTime < "08:00" ? "08:00" : timeOff.endTime;
    
            const startTimeMarker = calculateTimeMarker(startTime);
            const endTimeMarker = calculateTimeMarker(endTime);
            const timeSlot = TimeSlots.slice(startTimeMarker, endTimeMarker);
    
            const numOfTimetables = timetables.length;
    
            for (let i = 0; i < numOfTimetables; i++) {
                const timetable = timetables[i];
                const timeOffRegExp = new RegExp('[' + timeSlot + ']');
    
                if (timetable[weekday].match(timeOffRegExp)) {
                    timetables[i] = ['', '', '', '', '', ''];
                    sessionIDs[i] = '';
                }
            }
        }
    }
    timetables = timetables.filter(elem => elem !== ['', '', '', '', '', '']);
    sessionIDs = sessionIDs.filter(elem => elem !== '');

    console.log(timetables);
    console.log(sessionIDs);
    if (sessionIDs.length == 0) {
        let toast = new bootstrap.Toast(toastNoResult);
        toast.show();
    }
    else {
        let insertIndex = 0;
        while (insertIndex < sessionIDs.length && insertIndex < 10) {
            await fetch('/timetable', {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    "sessionIDs": sessionIDs[insertIndex]
                })
            });
            insertIndex++;
        }
        let res = await fetch('/timetableIndex')
        let index = await res.json();
        if (insertIndex == 1) {
            toastResult.querySelector('.toast-body').innerHTML = `Result is saved as Timetable#${index}`
        }
        else {
            toastResult.querySelector('.toast-body').innerHTML = `Result are saved as Timetable#${index + 1 - insertIndex}-${index}`
        }
        let toast = new bootstrap.Toast(toastResult);
        toast.show();
    }
});

function calculateTimeMarker(time) {
    if (time === null) return -1;

    const times = time.split(':');
    const hour = parseInt(times[0]);
    const minute = parseInt(times[1]);

    return Math.round(((hour * 60 + minute) / 30) - 16);
}

function addCourseToTimetables(timetables, sessionsIDs, courseTimeSlots) {
    const lectures = courseTimeSlots.lectures;
    const tutorials = courseTimeSlots.tutorials;
    const labs = courseTimeSlots.labs;
    const researches = courseTimeSlots.researches;

    const newTimetables = [];
    const newSessionIDs = [];

    let numOfTimeTables = timetables.length;
    for (let i = 0; i < numOfTimeTables; i++) {
        const timetable = timetables[i];
        const sessionID = sessionsIDs[i];

        loop1:
        for (let lecture of lectures) {
            const numOfSessions = lecture.sessionIDs.length;
            for (let j = 0; j < numOfSessions; j++) {
                const sessionWeekday = lecture.weekdays[j];
                const sessionTimeSlot = lecture.timeSlots[j];

                if (sessionTimeSlot === 'x') continue loop1;

                const sessionRegEx = new RegExp('[' + sessionTimeSlot + ']');
                if (timetable[sessionWeekday].match(sessionRegEx)) {
                    continue loop1;
                }
            }

            let timetableCopy = Object.assign([], timetable);
            let sessionIDCopy = Object.assign([], sessionID);

            for (let j = 0; j < numOfSessions; j++) {
                const sessionWeekday = lecture.weekdays[j];
                const sessionTimeSlot = lecture.timeSlots[j];
                const ID = lecture.sessionIDs[j];

                timetableCopy[sessionWeekday] = timetableCopy[sessionWeekday].concat(sessionTimeSlot);
                sessionIDCopy.push(ID);
            }

            newTimetables.push(timetableCopy);
            newSessionIDs.push(sessionIDCopy);
        }
    }

    let newTimetables2 = [];
    let newSessionIDs2 = [];

    if (!tutorials.length) {
        newTimetables2 = Object.assign([], newTimetables);
        newSessionIDs2 = Object.assign([], newSessionIDs);
    } else {
        numOfTimeTables = newTimetables.length;
        for (let i = 0; i < numOfTimeTables; i++) {
            const timetable = newTimetables[i];
            const sessionID = newSessionIDs[i];

            loop2:
            for (let tutorial of tutorials) {
                const numOfSessions = tutorial.sessionIDs.length;
                for (let j = 0; j < numOfSessions; j++) {
                    const sessionWeekday = tutorial.weekdays[j];
                    const sessionTimeSlot = tutorial.timeSlots[j];

                    if (sessionTimeSlot === 'x') continue loop2;

                    const sessionRegEx = new RegExp('[' + sessionTimeSlot + ']');
                    if (timetable[sessionWeekday].match(sessionRegEx)) {
                        continue loop2;
                    }
                }

                let timetableCopy = Object.assign([], timetable);
                let sessionIDCopy = Object.assign([], sessionID);

                for (let j = 0; j < numOfSessions; j++) {
                    const sessionWeekday = tutorial.weekdays[j];
                    const sessionTimeSlot = tutorial.timeSlots[j];
                    const ID = tutorial.sessionIDs[j];

                    timetableCopy[sessionWeekday] = timetableCopy[sessionWeekday].concat(sessionTimeSlot);
                    sessionIDCopy.push(ID);
                }

                newTimetables2.push(timetableCopy);
                newSessionIDs2.push(sessionIDCopy);
            }
        }
    }


    let newTimetables3 = [];
    let newSessionIDs3 = [];

    if (!labs.length) {
        newTimetables3 = Object.assign([], newTimetables2);
        newSessionIDs3 = Object.assign([], newSessionIDs2);
    } else {
        numOfTimeTables = newTimetables2.length;
        for (let i = 0; i < numOfTimeTables; i++) {
            const timetable = newTimetables2[i];
            const sessionID = newSessionIDs2[i];

            loop3:
            for (let lab of labs) {
                const numOfSessions = lab.sessionIDs.length;
                for (let j = 0; j < numOfSessions; j++) {
                    const sessionWeekday = lab.weekdays[j];
                    const sessionTimeSlot = lab.timeSlots[j];

                    if (sessionTimeSlot === 'x') continue loop3;

                    const sessionRegEx = new RegExp('[' + sessionTimeSlot + ']');
                    if (timetable[sessionWeekday].match(sessionRegEx)) {
                        continue loop3;
                    }
                }

                let timetableCopy = Object.assign([], timetable);
                let sessionIDCopy = Object.assign([], sessionID);

                for (let j = 0; j < numOfSessions; j++) {
                    const sessionWeekday = lab.weekdays[j];
                    const sessionTimeSlot = lab.timeSlots[j];
                    const ID = lab.sessionIDs[j];

                    timetableCopy[sessionWeekday] = timetableCopy[sessionWeekday].concat(sessionTimeSlot);
                    sessionIDCopy.push(ID);
                }

                newTimetables3.push(timetableCopy);
                newSessionIDs3.push(sessionIDCopy);
            }
        }
    }

    let newTimetables4 = [];
    let newSessionIDs4 = [];

    if (!researches.length) {
        newTimetables4 = Object.assign([], newTimetables3);
        newSessionIDs4 = Object.assign([], newSessionIDs3);
    } else {
        numOfTimeTables = newTimetables3.length;
        for (let i = 0; i < numOfTimeTables; i++) {
            const timetable = newTimetables3[i];
            const sessionID = newSessionIDs3[i];

            loop4:
            for (let research of researches) {
                const numOfSessions = research.sessionIDs.length;
                for (let j = 0; j < numOfSessions; j++) {
                    const sessionWeekday = research.weekdays[j];
                    const sessionTimeSlot = research.timeSlots[j];

                    if (sessionTimeSlot === 'x') continue loop4;

                    const sessionRegEx = new RegExp('[' + sessionTimeSlot + ']');
                    if (timetable[sessionWeekday].match(sessionRegEx)) {
                        continue loop4;
                    }
                }

                let timetableCopy = Object.assign([], timetable);
                let sessionIDCopy = Object.assign([], sessionID);

                for (let j = 0; j < numOfSessions; j++) {
                    const sessionWeekday = research.weekdays[j];
                    const sessionTimeSlot = research.timeSlots[j];
                    const ID = research.sessionIDs[j];

                    timetableCopy[sessionWeekday] = timetableCopy[sessionWeekday].concat(sessionTimeSlot);
                    sessionIDCopy.push(ID);
                }

                newTimetables4.push(timetableCopy);
                newSessionIDs4.push(sessionIDCopy);
            }
        }
    }

    return { timetables: newTimetables4, sessionIDs: newSessionIDs4 };
}

function modifyRefineModalSelect(courses, targetCard, ccArea) {
    const selectMenu = document.querySelector('#refine-modal .dropdown-menu');

    /* remove old list items */
    const oldItems = selectMenu.querySelectorAll('li');
    for (let i = 0; i < oldItems.length; i++) {
        oldItems[i].remove();
    }

    for (let course of courses) {
        const courseInfo = course.subject.toUpperCase() + ' ' + course.code.toUpperCase();

        if (!configurations.priorities[targetCard].some(course => course === courseInfo)) {
            selectMenu.insertAdjacentElement('beforeend', createDropDownItem(courseInfo, targetCard, ccArea));
        }
    }
}

function createDropDownItem(content = '', targetCard = '', ccArea = '') {
    const attributes = [];
    attributes.push(new Attribute('target-card', targetCard));
    attributes.push(new Attribute('cc-area', ccArea));

    const newItem = createHTMLElement('div', '', ['dropdown-item', 'no-select'], attributes, content, '');

    const itemAttributes = [];
    itemAttributes.push(new Attribute('data-bs-dismiss', 'modal'));
    itemAttributes.push(new Attribute('aria-label', 'Close'));

    return createHTMLElement('li', '', '', itemAttributes, '', newItem);
}

function createCourseCard(type, area, content = '?') {
    let cardTitle = '';
    const cardClasses = ['card', 'course-card', 'no-select']

    if (type === 'general') {
        cardTitle = 'Course'
        cardClasses.push(area);
    } else {
        cardTitle = 'Common Core';
        cardClasses.push(area);
    }

    const cardChildren = [];
    cardChildren.push(createCloseBtn());

    const cardTitleH5 = createHTMLElement('h5', '', ['card-title', 'mb-2'], '', cardTitle, '');
    const cardInfo = createHTMLElement('div', '', ['form-control', 'bg-light', 'course-info'], '', content, '');
    cardChildren.push(createHTMLElement('div', '', 'card-body', '', '', [cardTitleH5, cardInfo]));
    const card = createHTMLElement('div', '', cardClasses, '', '', cardChildren);

    return card;
}

function createFadedCourseCard(area, content = '?') {
    const newCard = createCourseCard('general', area, content);
    newCard.classList.add('faded');
    return newCard;
}

function createRowToRefine(card, targetCard, type) {
    if (!assertHTMLElement(card)) return;

    const commonCoreArea = createHTMLElement('div', '', ['course-cards-container', 'common-core-area'], '', '', card);

    const plusAttributes = [];
    plusAttributes.push(new Attribute('cc-area', type));
    plusAttributes.push(new Attribute('target-card', targetCard));

    const plusSign = createPlusBtn(plusAttributes);
    const plusBox = createHTMLElement('div', '', ['card', 'course-card', 'plus-sign'], '', '', plusSign);
    const commonCoreList = createHTMLElement('div', '', ['course-cards-container', 'common-core-list'], '', '', plusBox);

    const newRowAttributes = [];
    newRowAttributes.push(new Attribute('target-card', targetCard));
    newRowAttributes.push(new Attribute('cc-area', courseCardType[type]));

    const newRow = createHTMLElement('div', '', 'refine-flex-container', newRowAttributes, '', [commonCoreArea, commonCoreList]);
    return newRow;
}

function createCloseBtn() {
    const closeBtnClasses = ['bi', 'bi-x'];
    return createHTMLElement('i', '', closeBtnClasses, '', '', '');
}

function createPlusBtn(attributes = '') {
    const plusBtnClasses = ['bi', 'bi-plus-circle'];
    return createHTMLElement('i', '', plusBtnClasses, attributes, '', '');
}

function createRightArrow() {
    const rightArrowClasses = ['bi', 'bi-chevron-right'];
    return createHTMLElement('i', '', rightArrowClasses, '', '', '');
}

function createTimeRow() {
    const newId = 'time' + (nextTimeRow++);

    const closeBtn = createCloseBtn();
    closeBtn.setAttribute('target-time-row', newId);

    const weekDaySelect = createWeekDaySelect(newId);
    const startTimeSelect = createTimeSelect('start', newId);
    const endTimeSelect = createTimeSelect('end', newId);

    const newRow = createHTMLElement('div', newId, ['row', 'time-row'], '', '', [closeBtn, weekDaySelect, startTimeSelect, endTimeSelect]);
    return newRow;
}

function createWeekDaySelect(id) {
    const newSelect = createHTMLElement('div', '', ['col-sm-6', 'col-lg-4', 'time-item'], '', '', '');

    newSelect.innerHTML = `<div class="pill-container pill-container-shorten">
            <div class="badge rounded-pill bg-primary">Day</div>
        </div>
        <div class="dropdown-wrapper" wrapped="weekdays">
            <div class="dropdown">
                <button class="btn dropdown-toggle weekdays-button"
                    type="button" data-bs-toggle="dropdown" aria-expanded="false">
                    <text>Select weekday</text>
                </button>
                <ul class="dropdown-menu" aria-labelledby="weekdays-button">
                    <li>
                        <div class="dropdown-item" target-time-row="${id}" weekday-code="0">Monday</div>
                    </li>
                    <li>
                        <div class="dropdown-item" target-time-row="${id}" weekday-code="1">Tuesday</div>
                    </li>
                    <li>
                        <div class="dropdown-item" target-time-row="${id}" weekday-code="2">Wednesday</div>
                    </li>
                    <li>
                        <div class="dropdown-item" target-time-row="${id}" weekday-code="3">Thursday</div>
                    </li>
                    <li>
                        <div class="dropdown-item" target-time-row="${id}" weekday-code="4">Friday</div>
                    </li>
                </ul>
            </div>
        </div>`

    return newSelect;
}

function createTimeSelect(type, id) {
    const newTimeSelect = createHTMLElement('div', '', ['col-sm-3', 'col-lg-3', 'time-item'], '', '', '');

    newTimeSelect.innerHTML = `<div class="pill-container">
        <div class="badge rounded-pill bg-primary">${capitalizeWord(type)} time</div>
    </div>
    <input class="form-control" type="time" name="${type}-time" target-time-row="${id}" min="08:00" max="23:59">`

    return newTimeSelect;
}

function createCourseBadge(content, commonCoreArea, targetCard = '') {
    const newBadge = document.createElement('button');
    newBadge.classList.add('btn', 'modal-badge', 'no-select');
    if (commonCoreArea) {
        newBadge.classList.add(commonCoreArea);
    } else {
        newBadge.classList.add('general');
    }

    newBadge.setAttribute('data-bs-dismiss', 'modal');
    newBadge.setAttribute('aria-label', 'Close');
    newBadge.setAttribute('card-type', 'general');
    newBadge.setAttribute('cc-area', commonCoreArea);
    if (targetCard !== '') newBadge.setAttribute('target-card', targetCard);

    newBadge.innerHTML = content;

    return newBadge;
}