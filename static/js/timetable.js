import { getCourseList, renderCourse } from './modules/courseList.mjs';

let timetableIDs = []; //{timetable_id, student_id}[]
let timetableIndex = -1; //-1:default 0:null 1:1st timetable
let sessionIDs = []; //current timetable_sessionIDs
let timetableSaveBool = 1; //0:unsaved 1:saved
let selectedCourseTitles = []; //{subject_id, course_code}
let selectedCourseList = [];
let starredCourseTitles = []; //{subject_id, course_code}
let starredCourseList = [];
let infoCourseList = [];

const retrieve = document.querySelector('#retrieve');
const title = document.querySelector('#title');
const selected = document.querySelector('#selected');
const starred = document.querySelector('#starred');
const info = document.querySelector('#info');
const divInserts = document.querySelectorAll('.timeslot');
const delConfirm = document.querySelector('#delConfirm')

const deleteModel = new bootstrap.Modal(document.getElementById('delAlert'))
const toastUnsave = document.getElementById('toastUnsave');
const toastSave = document.getElementById('toastSave');
const toastCreateSave = document.getElementById('toastCreateSave');
const toastCreate = document.getElementById('toastCreate');
const toastDelete = document.getElementById('toastDelete');
const toastDeleteError = document.getElementById('toastDeleteError');
const toastAddError =  document.getElementById('toastAddError');
const toastAdd =  document.getElementById('toastAdd');
const toastRemove =  document.getElementById('toastRemove');


window.onload = () => {
    loadTimetable(timetableIndex);
    deleteAddEventListener();
    AOS.init();
    // await popoversInit();//bootstrap popovers init 
}

async function loadTimetable(index) {
    for(let divInsert of divInserts){
        divInsert.innerHTML = "";
    }    
    let res = await fetch('/timetableIDs');
    timetableIDs = await res.json();
    if (index == -1) {
        timetableIndex = timetableIDs.length;
    }
    title.innerHTML = '';
    retrieve.innerHTML = '<li><a id="create" class="dropdown-item href="#">New timetable</a></li>';
    if (timetableIndex == 0) {
        timetableSaveBool = 0;
        title.innerHTML += `Get Started`;
    }
    else {
        retrieve.innerHTML += '<li><hr class="dropdown-divider"></li>'
        timetableSaveBool = 1;
        title.innerHTML = `Timetable #${timetableIndex}`;
        for (let i = 1; i <= timetableIDs.length; i++) {
            if (i == timetableIndex) {
                retrieve.innerHTML += `
                    <li><a class="dropdown-item disabled" href="#">Timetable #${i}</a></li>
                `;
                continue;
            }
            retrieve.innerHTML += `
                <li><a class="dropdown-item select" href="#">Timetable #${i}</a></li>
            `;
        }
        const selectLis = document.querySelectorAll('.select');
        for (let selectLi of selectLis) {
            selectLi.addEventListener('click', function (event) {
                if (timetableSaveBool == 0 && timetableIndex > 0) {
                    let toast = new bootstrap.Toast(toastUnsave);
                    toast.show();
                    return;
                }
                timetableIndex = event.target.innerHTML.split("#")[1];
                resetSessionIDs();
                loadTimetable(timetableIndex);
            })
        }
        sessionIDs = [];
        sessionIDs = await getSessionIDsFromTimetableID(timetableIDs[timetableIndex - 1]);
        for (let sessionID of sessionIDs) {
            putSessionIntoTable(sessionID);
        }
    }
    createAddEventListener();
    info.innerHTML = '';
    infoCourseList = [];
    await loadSelectedCourse();
    await loadStarredCourse();
    await addEventListenerToInfoModels();
}

async function loadSelectedCourse() {
    selectedCourseTitles = [];
    for (let sessionID of sessionIDs) {
        let activity = await getActivityFromSessionID(sessionID);
        let course = await getCourseFromCourseID(activity.course_id);
        if (selectedCourseTitles.length == 0) {
            selectedCourseTitles.push({
                subject_id: course.subject_id,
                course_code: course.course_code
            })
        }
        else {
            for (let index in selectedCourseTitles) {
                if (selectedCourseTitles[index].subject_id == course.subject_id && selectedCourseTitles[index].course_code == course.course_code) {
                    break;
                }
                if (index == selectedCourseTitles.length-1) {
                    selectedCourseTitles.push({
                        subject_id: course.subject_id,
                        course_code: course.course_code
                    });
                    break;
                }
                continue;
            }
        }
    }
    selected.innerHTML = ``;
    selectedCourseList = await getCourseList(selectedCourseTitles)
    if (selectedCourseList.length == 0) {
        selected.innerHTML = `
            <li><a class="dropdown-item disabled">No saved courses</a></li>
        `;
        return;
    }
    for (let index in selectedCourseTitles) {
        let course = selectedCourseTitles[index].subject_id.toUpperCase() + selectedCourseTitles[index].course_code;
        selected.innerHTML += `
            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#${course}">${course}</a></li>
        `;
        info.innerHTML += await setInfoInnerHTML(course, selectedCourseList, index);
        infoCourseList.push(selectedCourseList[index])
    }
}

async function loadStarredCourse() {
    starredCourseTitles = [];
    let res = await fetch('/starredCourses');
    let result = await res.json();
    for (let course of result.courses) {
        starredCourseTitles.push({
            'subject_id': course.subject,
            'course_code': course.code
        })
    }
    starredCourseList = await getCourseList(starredCourseTitles);
    starred.innerHTML = '';
    if (starredCourseTitles.length == 0) {
        starred.innerHTML += `
            <li><a class="dropdown-item" href="/courses-page">Add courses from courses page</a></li>
        `;
        return;
    }
    for (let index in starredCourseTitles) {
        let course = starredCourseTitles[index].subject_id.toUpperCase() + starredCourseTitles[index].course_code;
        starred.innerHTML += `
            <li><a class="dropdown-item" data-bs-toggle="modal" data-bs-target="#${course}">${course}</a></li>
        `;
        const courseEle = document.getElementById(`${course}`);
        if (courseEle != null) {
            continue;
        }
        info.innerHTML += await setInfoInnerHTML(course, starredCourseList, index);
        infoCourseList.push(starredCourseList[index])
    }
};

async function setInfoInnerHTML(course, courseList, index) {
    let courseSessionIDsArray = await getSessionIDsFromCourse(course);
    return `
        <div class="modal infoModal fade" id="${course}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-course modal-dialog-centered modal-dialog-scrollable">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">
                            <div>${course}</div> 
                            <div>${courseList[index].title}</div>
                            <div>${courseList[index].credit} credit(s)</div>
                        </h5>
                        <button type="button" class="modalClose btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body" id="${course}body">
                        Select session(s):
                        ${renderCourse(courseList[index], courseSessionIDsArray, sessionIDs)}
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="modalUnselectAll btn btn-secondary">Reset</button>
                        <button type="button" class="modalSave btn btn-primary" data-bs-dismiss="modal">OK</button>
                    </div>
                </div>
            </div>
        </div>
    `;
}

async function addEventListenerToInfoModels() {
    for (let course of infoCourseList) {
        let courseSessionIDsArray = await getSessionIDsFromCourse(course.subject + course.code);
        let courseName = course.subject.toUpperCase() + course.code;

        const modalDiv = document.querySelector(`#${courseName}`)
        const modalCloseButton = modalDiv.querySelector('.modalClose');
        const courseBody = document.querySelector(`#${courseName}body`);

        modalCloseButton.addEventListener('click', () => {
            courseBody.innerHTML = `
                Select session(s):
                ${renderCourse(course, courseSessionIDsArray, sessionIDs)}
            `
            modalDiv.querySelector('.modalSave').removeEventListener('click', modalSaveAddEventListenerFunction(modalDiv, courseSessionIDsArray));
        });

        const modalUnselectAllButton = modalDiv.querySelector('.modalUnselectAll');
        modalUnselectAllButton.addEventListener('click', () => {
            courseBody.innerHTML = `
                Select session(s):
                ${renderCourse(course, courseSessionIDsArray, [])}
            `
            modalDiv.querySelector('.modalSave').removeEventListener('click', modalSaveAddEventListenerFunction(modalDiv, courseSessionIDsArray));
        });

        modalSaveAddEventListener(modalDiv, courseSessionIDsArray)
    }
}

function modalSaveAddEventListener(modalDiv, courseSessionIDsArray) {
    const modalSaveButton = modalDiv.querySelector('.modalSave');
    modalSaveButton.addEventListener('click', modalSaveAddEventListenerFunction(modalDiv, courseSessionIDsArray));
}

function modalSaveAddEventListenerFunction(modalDiv, courseSessionIDsArray) {
    return async(event) => {
        const modalCheckboxes = modalDiv.querySelectorAll('.form-check-input')
        let index = 0;
        let modalCheckboxIndexArray = [-1, -1, -1, -1]
        for (let modalCheckbox of modalCheckboxes) {
            let modalCheckboxName = modalCheckbox.getAttribute('name')
            let modalCheckboxNameIndex = modalCheckboxName.split('#')[1]
            if(modalCheckboxIndexArray[modalCheckboxNameIndex] == -1){
                modalCheckboxIndexArray[modalCheckboxNameIndex] = 0;
            }            
            if (modalCheckbox.checked){
                modalCheckboxIndexArray[modalCheckboxNameIndex] = 1;
            }
        }
        console.log(modalCheckboxIndexArray)
        if(modalCheckboxIndexArray.includes(0) && modalCheckboxIndexArray.includes(1)){
            let message1 = []
            let message2 = ' is(are) not selected';            
            if(modalCheckboxIndexArray[0] == 0){
                message1.push('Lecture')
            }
            if(modalCheckboxIndexArray[1] == 0){
                message1.push('Tutorial')
            }
            if(modalCheckboxIndexArray[2] == 0){
                message1.push('Lab')
            }
            if(modalCheckboxIndexArray[3] == 0){
                message1.push('Research')
            }
            let message = message1.join(', ') + message2
            toastAddError.querySelector('.toast-body').innerHTML = `${message}`
            let toast = new bootstrap.Toast(toastAddError);
            toast.show();
            return;
        }
        else if(modalCheckboxIndexArray.includes(1)){
            let toast = new bootstrap.Toast(toastAdd);
            toast.show();
        }
        else{
            let toast = new bootstrap.Toast(toastRemove);
            toast.show();
        }
        timetableSaveBool = 0;        
        for (let modalCheckbox of modalCheckboxes) {
            if (modalCheckbox.checked) {
                for (let courseSessionID of courseSessionIDsArray[index]) {
                    if (!sessionIDs.includes(courseSessionID)) {
                        sessionIDs.push(courseSessionID);
                        putSessionIntoTable(courseSessionID);
                    }
                }
            }
            else {
                for (let courseSessionID of courseSessionIDsArray[index]) {
                    for (let index in sessionIDs) {
                        if (sessionIDs[index] == courseSessionID) {
                            sessionIDs.splice(index, 1);
                            document.querySelector(`#sessionDiv${courseSessionID}`).remove();
                        }
                    }
                }
            }
            index++;
        }        
    }
}

function popoversInit() {
    const popoverTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="popover"]'))
    const popoverList = popoverTriggerList.map(function (popoverTriggerEl) {
        return new bootstrap.Popover(popoverTriggerEl)
    })
    const popover = new bootstrap.Popover(document.querySelector('.popover-dismiss'), {
        trigger: 'focus'
    })
}

async function createAddEventListener() {
    document.querySelector('#create').addEventListener('click', async function () {
        if (timetableSaveBool == 0 && timetableIndex > 0) {
            let toast = new bootstrap.Toast(toastUnsave);
            toast.show();
            return;
        }
        if (timetableIndex != 0) {
            resetSessionIDs();
        }
        let res = await fetch('/timetable', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "sessionIDs": sessionIDs
            })
        });
        let timetableID = await res.json();
        timetableIDs.push(timetableID);
        if (timetableIndex == 0) {
            let toast = new bootstrap.Toast(toastCreateSave);
            toast.show();
            timetableIndex = 1;
            timetableSaveBool = 1;
            title.innerHTML = `Timetable #${timetableIndex}`;
            retrieve.innerHTML = `
                    <li><a id="create" class="dropdown-item href="#">New timetable</a></li>
                    <li><hr class="dropdown-divider"></li>
                    <li><a class="dropdown-item disabled" href="#">Timetable #1</a></li>
                `;
            createAddEventListener();
        }
        else {
            let toast = new bootstrap.Toast(toastCreate);
            toast.show();
            timetableIndex = timetableIDs.length;
            loadTimetable(timetableIndex)
        }
    });
}

document.querySelector('#save').addEventListener('click', async function () {
    if (timetableSaveBool == 1) {
        let toast = new bootstrap.Toast(toastSave);
        toast.show();
        return;
    }
    if (timetableIndex == 0) {
        let toast = new bootstrap.Toast(toastCreateSave);
        toast.show();
        timetableIndex = 1;
        timetableSaveBool = 1;
        let res = await fetch('/timetable', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "sessionIDs": sessionIDs
            })
        });
        retrieve.innerHTML = `
                <li><a id="create" class="dropdown-item href="#">New timetable</a></li>
                <li><hr class="dropdown-divider"></li>
                <li><a class="dropdown-item disabled" href="#">Timetable #1</a></li>
            `;
        createAddEventListener();
        let timetableID = await res.json();
        timetableIDs.push(timetableID);
    }
    else {
        let toast = new bootstrap.Toast(toastSave);
        toast.show();
        await fetch('/timetable', {
            method: "PUT",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                "timetableIndex": timetableIndex - 1,
                "sessionIDs": sessionIDs
            })
        });
    }
    title.innerHTML = `Timetable #${timetableIndex}`;
    timetableSaveBool = 1;
    loadTimetable(timetableIndex);
});

document.querySelector('#delete').addEventListener('click', async function () {
    if (timetableIndex == 0) {
        if (sessionIDs[0] == undefined) {
            let toast = new bootstrap.Toast(toastDeleteError);
            toast.show();
            return;
        }
    }
    deleteModel.show();
});

function deleteAddEventListener() {
    delConfirm.addEventListener('click', async() => {
        let toast = new bootstrap.Toast(toastDelete);
        toast.show();
        resetSessionIDs();
        if (timetableIndex == 0) {
            return;
        }
        else {
            await fetch('/timetable', {
                method: "DELETE",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    "timetableIndex": timetableIndex - 1
                })
            });
            timetableIndex--;
            loadTimetable(-1);
        }
    });
}

async function putSessionIntoTable(sessionID) {
    let session = await getSessionFromSessionID(sessionID);
    let activity = await getActivityFromSessionID(sessionID);
    let course = await getCourseFromCourseID(activity.course_id);
    let subject = await getSubjectFromSubjectID(course.subject_id);
    let department = await getDepartmentFromDepartmentID(subject.department_id);
    let courseName = course.subject_id.toUpperCase() + course.course_code;
    let startTime = session.start_time.split(":");
    let endTime = session.end_time.split(":");
    let divLocation = session.weekday + (startTime[0] - 8) * 14 + Math.floor(startTime[1] / 30) * 7;
    const divInsert = divInserts[divLocation];
    if (screen.width < 960) {
        divInsert.innerHTML = "";
        divInsert.innerHTML = `
        <div id="sessionDiv${sessionID}">
            <div>${course.subject_id.toUpperCase()}</div>
            <div>${course.course_code}</div>
        </div>
    `;
    }
    else{
        divInsert.innerHTML = "";
        divInsert.innerHTML = `
            <div id="sessionDiv${sessionID}">
                <div>${courseName} ${getActivityNameFromActivity(activity)}</div>
                <div>${startTime[0]}:${startTime[1]}-${endTime[0]}:${endTime[1]}</div>
            </div>
        `;
    }
    if (department.department_id == 1) {
        document.querySelector(`#sessionDiv${sessionID}`).setAttribute('class', 'accent-purple-gradient sessionDiv');
    }
    else if (department.department_id == 2) {
        document.querySelector(`#sessionDiv${sessionID}`).setAttribute('class', 'accent-blue-gradient sessionDiv');
    }
    else if (department.department_id == 3) {
        document.querySelector(`#sessionDiv${sessionID}`).setAttribute('class', 'accent-orange-gradient sessionDiv');
    }
    else if (department.department_id == 4) {
        document.querySelector(`#sessionDiv${sessionID}`).setAttribute('class', 'accent-green-gradient sessionDiv');
    }
    else {
        document.querySelector(`#sessionDiv${sessionID}`).setAttribute('class', 'accent-cyan-gradient sessionDiv');
    }
    const sessionDiv = document.querySelector(`#sessionDiv${sessionID}`);
    let sessionDivHeight = Math.floor((endTime[0] - startTime[0]) * 200 + (endTime[1] - startTime[1]) * 100 / 30);
    let sessionDivTransform = Math.floor(startTime[1] % 30 * 37);
    sessionDiv.style.height = `${sessionDivHeight}%`;
    sessionDiv.style.transform = `translateY(${sessionDivTransform}px)`;
}

function resetSessionIDs() {
    for (let sessionID of sessionIDs) {
        document.getElementById(`sessionDiv${sessionID}`).remove();
    }
    sessionIDs = [];
}


async function getSessionIDsFromTimetableID(timetableID) {
    let res = await fetch(`/sessionIDs?timetableID=${timetableID}`);
    let sessionIDs = await res.json();
    return sessionIDs;
}

async function getSessionIDsFromCourse(course) {
    let res = await fetch(`/sessionIDs?course=${course}`);
    let sessionIDs = await res.json();
    return sessionIDs;
}

async function getSessionFromSessionID(sessionID) {
    let res = await fetch(`/session?sessionID=${sessionID}`);
    let session = await res.json();
    return session;
}

async function getActivityFromSessionID(sessionID) {
    let res = await fetch(`/activity?sessionID=${sessionID}`);
    let activity = await res.json();
    return activity;
}

async function getCourseFromCourseID(courseID) {
    let res = await fetch(`/course?courseID=${courseID}`);
    let course = await res.json();
    return course;
}

async function getSubjectFromSubjectID(subjectID) {
    let res = await fetch(`/subject?subjectID=${subjectID}`);
    let subject = await res.json();
    return subject;
}

async function getDepartmentFromDepartmentID(departmentID) {
    let res = await fetch(`/department?departmentID=${departmentID}`);
    let department = await res.json();
    return department;
}

function getActivityNameFromActivity(activity) {
    if (activity.lecture_id) {
        return activity.lecture_name;
    }
    else if (activity.tutorial_id) {
        return activity.tutorial_name;
    }
    else if (activity.lab_id) {
        return activity.lab_name;
    }
    else if (activity.research_id) {
        return activity.research_name;
    }
    return null;
}