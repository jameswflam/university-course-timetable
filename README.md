# Tecky WSP Project - Timetable app

## Useful link
- [UST course list page](https://w5.ab.ust.hk/wcq/cgi-bin/2040/subject/HUMA)
- [UST course catalogue](https://prog-crs.ust.hk/ugcourse)

## Definition of application 
A timetable app for university students. This will greatly reduce the time and effort required for students to register courses. Once preferences and requirements are set, the application will automatically generate viable registration plans for students. Students can edit the timetable and save all plans in the application. Students will be able to enroll the courses in this application as well.

There are two means for the application to get university courses data:
1. By web-scraping of university course information site
2. By instructors manual upload

Currently, the universities in Hong Kong does not provide API for course information. Once this application is complete and prove to be a useful tool for students, we could proceed to request API service from universities.

## Nomenclature
Most universities in Hong Kong requires undergrads student to complete a set of studies that can be grouped as below:
1. Course code<br>
    Below is a common course code
    > ACCT 1110 Accounting, Business and Society (Business & Management)

    `ACCT` is the **COURSE SUBJECT**<br>
    `1110` is the **COURSE CODE**<br>
    `Accounting, Business and Society` is the **COURSE TITLE**<br>

    **DEPARTMENT** is a broad branch of study, e.g. Engineering, Business, Law, Architecture
    **DISCIPLINE** is a sub-group of department, e.g. Mechanical Engineering, BBA, Quantitative Finance
    
2. Common core courses<br>
    **COMMON CORE COURSES** are mandated courses from a broad spectrum of disiplines<br>
    Common core courses are catagorized into different **COMMON CORE AREA**<br>
    See below for UST's common core example:<br>
    <img src="https://uce.ust.hk/web/images/Figure%202_Credit%20Requirements%20of%20the%20Common%20Core%20in%204-Year%20Program.jpg" />

3. Language courses<br>
    Universites in Hong Kong mandates English course for all students. Students are required to take one more languguage course (usually Chinese for Chinese-speaking students)

4. Department general courses<br>
    **DEPARTMENT GENERAL COURSES** are mandated courses which are department-specific.

5. Discipline core courses<br>
    **DISCIPLINE CORE COURSES** are mandated courses which are discipline-specific.

6. Discipline elective courses<br>
    **DISCIPLINE ELECTIVE COURSES** are optional courses which are discipline-specific.

7. Capstone courses<br>
    **CAPSTONE COURSES** are mandated courses that is project based. Usually each student will enroll in one capstone course in his/her final year of study.

## Course grouping
### Schools: 
- School of Science `0`
- School of Engineering `1`
- School of Business and Management `2`
- School of Humanities and Social Science `3`
- Other `4`

### Subjects:
- ACCT `0`
- BIBU `1`
- BIEN `1`
- CENG `1`
- CHEM `0`
- CIVL `1`
- COMP `1`
- CPEG `1`
- DSCT `4`
- ECON `2`
- ELEC `1`
- ENEG `1`
- ENGG `1`
- ENTR `2`
- ENVR `0`
- ENVS `0`
- FINA `2`
- GBUS `2`
- GNED `4`
- HART `3`
- HLTH `4`
- HUMA `3`
- IEDA `1`
- IIMP `4`
- ISDN `1`
- ISOM `2`
- LABU `2`
- LANG `4`
- LIFS `0`
- MARK `2`
- MATH `0`
- MECH `1`
- MGMT `2`
- OCES `0`
- PHYS `0`
- PPOL `3`
- RMBI `2`
- SBMT `2`
- SCIE `0`
- SHSS `4`
- SOSC `3`
- SUST `0`
- TEMG `1`
- UROP `4`
- WBBA `2`

## Features
- Timetable settings
    - [ ] Students can define time-slot preferences
    - [ ] Students can define must-include courses
    - [ ] Students can define must-include common core area
    - [ ] Students can define courses preference

- UI/IX:
    - [ ] Items on timetable can be dragged and dropped
    - [ ] The application should be seamless, without extensive reloading of page
    - [ ] Color grouping of common cores, department general courses, and discipline core courses

- Account settings
    - [ ] Change password

- Functionality
    - [ ] Multiple timetable plans can be saved and retrieved
    - [ ] Different plans can be generated according to student-defined options
    - [ ] Students can search courses by department, course-subject, course-code, common core area
    - [ ] Students can see the registered course in a list/table
    - [ ] Students can see information of all courses in a course list page