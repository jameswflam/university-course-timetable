import { client } from './app';

class BasicFunctions {
    constructor() {}

    capitalizeString(string: string) {
        let words = string.split(' ');
        const exceptions = ['a', 'an', 'the', 'and', 'to'];
        
        words = words.map((word, index) => {
            if (index === 0) return this.capitalizeWord(word);
    
            if (exceptions.some(exception => word === exception)) return word;
    
            return this.capitalizeWord(word);
        })
    
        return words.join(' ');
    }

    capitalizeWord(word: string) {
        return word[0].toUpperCase() + word.slice(1);
    }
}

interface DynamicObject {
    [key: string]: any
}

interface Activities extends DynamicObject {
    [key: string]: Activity[];
}

export class Course extends BasicFunctions {
    private id: number;
    private subject: string;
    private code: string;
    private title: string;
    private credit: number;
    private lectures: Activities;
    private tutorials: Activities;
    private labs: Activities;
    private researches: Activities;
    private commonCoreArea: string | null;

    constructor(id: number, subject: string, code: string, title: string, credit: number, commonCoreArea: string | null) {
        super();
        this.id = id;
        this.subject = subject;
        this.code = code;
        this.title = title;
        this.credit = credit;
        this.lectures = {};
        this.tutorials = {};
        this.labs = {};
        this.researches = {};
        this.commonCoreArea = commonCoreArea;
    }

    public appendLecture(activity: Activity) {
        const name = activity.getName();

        if (!this.lectures[name]) this.lectures[name] = [];
        this.lectures[name].push(activity);
    }

    public appendTutorial(activity: Activity) {
        const name = activity.getName();

        if (!this.tutorials[name]) this.tutorials[name] = [];
        this.tutorials[name].push(activity);
    }

    public appendLab(activity: Activity) {
        const name = activity.getName();

        if (!this.labs[name]) this.labs[name] = [];
        this.labs[name].push(activity);
    }

    public appendResearch(activity: Activity) {
        const name = activity.getName();

        if (!this.researches[name]) this.researches[name] = [];
        this.researches[name].push(activity);
    }

    public getCourseCode() {
        return this.subject.toUpperCase() + ' ' + this.code;
    }

    public getTitle() {
        return this.capitalizeString(this.title);
    }

    public getCredit() {
        return this.credit;
    }

    public getId() {
        return this.id;
    }

    public getCommonCoreArea() {
        return this.commonCoreArea;
    }
}

export enum ActivityType {
    Lecture = 0,
    Tutorial,
    Lab,
    Research
}

export enum WeekDays {
    Mon = 0,
    Tue,
    Wed,
    Thu,
    Fri
}

export class Activity extends BasicFunctions{
    private type: ActivityType;
    private name: string;
    private date: string | null;
    private startTime: string | null;
    private endTime: string | null;
    private venue: string | null;
    private instructorFirstName: string | null;
    private instructorLastName: string | null;
    private sessionID: number | undefined;

    constructor(type: ActivityType, name: string, date: WeekDays, startTime:string, endTime:string, venue:string, instructorFirstName: string, instructorLastName: string, sessionID: number | undefined = undefined) {
        super();
        this.type = type;
        this.name = name;
        this.date = WeekDays[date];
        this.startTime = startTime;
        this.endTime = endTime;
        this.venue = venue;
        this.instructorFirstName = instructorFirstName;
        this.instructorLastName = instructorLastName;
        this.sessionID = sessionID;
    }

    public getFullName() {
        let regEXPrefix = '';
        switch (this.type) {
            case ActivityType.Lecture:
                regEXPrefix = 'L';
                break;

            case ActivityType.Tutorial:
                regEXPrefix = 'T';
                break;
            
            case ActivityType.Lab:
                regEXPrefix = 'LA';
                break;
            
            case ActivityType.Research:
                regEXPrefix = 'R';
                break;
            
            default:
                regEXPrefix = ' ';
        }

        const regexExpression = new RegExp ('^' + regEXPrefix + '(.*)$');

        const numbersMatchedArray = this.name.match(regexExpression);

        let numbers: string;
        if (numbersMatchedArray) {
            numbers = ' ' + numbersMatchedArray[1];
        } else {
            numbers = '';
        }

        return ActivityType[this.type] + numbers;
    }

    public getType() {
        return this.type;
    }

    public getName() {
        return this.name;
    }

    public getDate() {
        return this.date;
    }

    public getTime() {
        return this.startTime + '-' + this.endTime;
    }

    public getVenue() {
        return this.venue;
    }

    public getInstructor() {
        return (this.instructorFirstName + ' ' + this.instructorLastName);
    }

    public getSessionID() {
        return this.sessionID;
    }
}

export async function generateCourseJSON(courses: any[]) {
    let courseObjs: Course[] = [];

    for (let course of courses) {
        const courseObj = new Course(course.id, course.subject, course.code, course.title, course.credit, course.common_core_area);

        /* get all lectures of the current course */
        const lectures = (await client.query(`SELECT lectures.lecture_id as id, lectures.lecture_name as name, sessions.weekday as date,
            sessions.start_time as start_time, sessions.end_time as end_time, sessions.venue as venue,
            instructors.first_name as first_name, instructors.last_name as last_name, sessions.session_id as session_id
            FROM lectures
            JOIN courses ON lectures.course_id = courses.course_id
            JOIN sessions ON lectures.lecture_id = sessions.lecture_id
            JOIN instructors ON instructors.instructor_id = lectures.instructor_id
            WHERE courses.course_id = $1;`, [course.id])).rows;

        for (let lecture of lectures) {
            const lectureObj = new Activity(ActivityType.Lecture, lecture.name, lecture.date,
                lecture.start_time, lecture.end_time, lecture.venue, lecture.first_name, lecture.last_name, lecture.session_id);

            courseObj.appendLecture(lectureObj);
        }

        /* get all tutorials of the current course */
        const tutorials = (await client.query(`SELECT tutorials.tutorial_id as id, tutorials.tutorial_name as name, sessions.weekday as date,
            sessions.start_time as start_time, sessions.end_time as end_time, sessions.venue as venue,
            instructors.first_name as first_name, instructors.last_name as last_name, sessions.session_id as session_id
            FROM tutorials
            JOIN courses ON tutorials.course_id = courses.course_id
            JOIN sessions ON tutorials.tutorial_id = sessions.tutorial_id
            JOIN instructors ON instructors.instructor_id = tutorials.instructor_id
            WHERE courses.course_id = $1;`, [course.id])).rows;

        for (let tutorial of tutorials) {
            const tutorialObj = new Activity(ActivityType.Tutorial, tutorial.name, tutorial.date,
                tutorial.start_time, tutorial.end_time, tutorial.venue, tutorial.first_name, tutorial.last_name, tutorial.session_id);

            courseObj.appendTutorial(tutorialObj);
        }

        /* get all labs of the current course */
        const labs = (await client.query(`SELECT labs.lab_id as id, labs.lab_name as name, sessions.weekday as date,
            sessions.start_time as start_time, sessions.end_time as end_time, sessions.venue as venue,
            instructors.first_name as first_name, instructors.last_name as last_name, sessions.session_id as session_id
            FROM labs
            JOIN courses ON labs.course_id = courses.course_id
            JOIN sessions ON labs.lab_id = sessions.lab_id
            JOIN instructors ON instructors.instructor_id = labs.instructor_id
            WHERE courses.course_id = $1;`, [course.id])).rows;

        for (let lab of labs) {
            const labObj = new Activity(ActivityType.Lab, lab.name, lab.date,
                lab.start_time, lab.end_time, lab.venue, lab.first_name, lab.last_name, lab.session_id);

            courseObj.appendLab(labObj);
        }

        /* get all researches of the current course */
        const researches = (await client.query(`SELECT researches.research_id as id, researches.research_name as name, sessions.weekday as date,
            sessions.start_time as start_time, sessions.end_time as end_time, sessions.venue as venue,
            instructors.first_name as first_name, instructors.last_name as last_name, sessions.session_id as session_id
            FROM researches
            JOIN courses ON researches.course_id = courses.course_id
            JOIN sessions ON researches.research_id = sessions.research_id
            JOIN instructors ON instructors.instructor_id = researches.instructor_id
            WHERE courses.course_id = $1;`, [course.id])).rows;

        for (let research of researches) {
            const researchObj = new Activity(ActivityType.Research, research.name, research.date,
                research.start_time, research.end_time, research.venue, research.first_name, research.last_name, research.session_id);

            courseObj.appendResearch(researchObj);
        }

        courseObjs.push(courseObj);
    }

    return courseObjs;
}