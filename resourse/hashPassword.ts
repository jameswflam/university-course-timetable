import bcrypt from 'bcryptjs';
const SALT_ROUND = 10 ;

async function hashPassword(plainPassword:string){
    return await bcrypt.hash(plainPassword, SALT_ROUND);
}
async function main() {
    let password = await hashPassword("password");
    console.log(password)
}
main()
