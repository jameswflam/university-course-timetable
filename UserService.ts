import { Client } from "pg";
import { Request } from "express";

export class UserService{
    constructor(private client:Client){}
    
    async getUser(username:string){
        return (await this.client.query('SELECT * from students where username = $1', [username])).rows;
    }

    async getUserInfo(req:Request){
        return (await this.client.query('SELECT student_id, first_name, last_name, password FROM students where student_id = $1', [req.session['student_id']])).rows[0];
    }

    async setStudentInfo(first_name:string, last_name:string, passwordHash:string, req:Request){
        await this.client.query('UPDATE students SET first_name = $1, last_name = $2, password = $3  where student_id = $4', [first_name, last_name, passwordHash, req.session['student_id']]);
    }

    async createUser(first_name:string, last_name:string, username:string, passwordHash:string){
        await this.client.query('INSERT into students(first_name, last_name, username, password) VALUES ($1, $2 , $3, $4)', [first_name, last_name, username, passwordHash]);
    }

    async getPasswordHash(req:Request){
        return (await this.client.query('SELECT password FROM students WHERE student_id= $1', [req.session['student_id']])).rows[0];
    }    
}