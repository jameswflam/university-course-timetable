import express from "express";
import { userController } from "./app";
import { isLoggedIn } from "./guard";

export const routes = express.Router();

//userController
routes.get('/', userController.renderIndexPage);
routes.get('/login', userController.renderLoginPage);
routes.get('/signup', userController.renderSignUpPage);
routes.get('/configure', isLoggedIn, userController.renderConfigPage);
routes.get('/registered', userController.renderRegisteredPage);
routes.get('/editprofile', isLoggedIn, userController.renderEditProfilePage);
routes.post('/login', userController.login);
routes.post('/signup', userController.signup);
routes.get('/logout', userController.logout);
routes.get('/info', userController.studentInfo);
routes.put('/updateStudentInfo', userController.updateStudentInfo);